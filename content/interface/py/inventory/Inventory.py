# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'content\interface\ui\inventory\inventory.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_WidgetInventory(object):
    def setupUi(self, WidgetInventory):


        WidgetInventory.setObjectName("WidgetInventory")
        WidgetInventory.resize(445, 427)
        WidgetInventory.setAutoFillBackground(False)
        WidgetInventory.setStyleSheet("")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(WidgetInventory)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.widget = QtWidgets.QWidget(WidgetInventory)
        self.widget.setObjectName("widget")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.qtabinv = QtWidgets.QTabWidget(self.widget)
        self.qtabinv.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.qtabinv.setObjectName("qtabinv")
        self.tab_pokeball = QtWidgets.QWidget()
        self.tab_pokeball.setAccessibleName("")
        self.tab_pokeball.setObjectName("tab_pokeball")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.tab_pokeball)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableWidget = QtWidgets.QTableWidget(self.tab_pokeball)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.verticalLayout.addWidget(self.tableWidget)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../image/icon/pokeball.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_pokeball, icon, "")
        self.tab_potion = QtWidgets.QWidget()
        self.tab_potion.setObjectName("tab_potion")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_potion)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.tableWidget_2 = QtWidgets.QTableWidget(self.tab_potion)
        self.tableWidget_2.setObjectName("tableWidget_2")
        self.tableWidget_2.setColumnCount(0)
        self.tableWidget_2.setRowCount(0)
        self.verticalLayout_2.addWidget(self.tableWidget_2)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../../image/icon/potion.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_potion, icon1, "")
        self.tab_rare = QtWidgets.QWidget()
        self.tab_rare.setObjectName("tab_rare")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.tab_rare)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.tableWidget_3 = QtWidgets.QTableWidget(self.tab_rare)
        self.tableWidget_3.setObjectName("tableWidget_3")
        self.tableWidget_3.setColumnCount(0)
        self.tableWidget_3.setRowCount(0)
        self.verticalLayout_6.addWidget(self.tableWidget_3)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../../image/icon/rare.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_rare, icon2, "")
        self.verticalLayout_5.addWidget(self.qtabinv)
        self.verticalLayout_3.addWidget(self.widget)
        self.widget_2 = QtWidgets.QWidget(WidgetInventory)
        self.widget_2.setObjectName("widget_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.pushButton_close = QtWidgets.QPushButton(self.widget_2)
        self.pushButton_close.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../../image/icon/close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_close.setIcon(icon3)
        self.pushButton_close.setIconSize(QtCore.QSize(80, 20))
        self.pushButton_close.setObjectName("pushButton_close")
        self.horizontalLayout.addWidget(self.pushButton_close)
        self.verticalLayout_3.addWidget(self.widget_2)

        self.retranslateUi(WidgetInventory)
        self.qtabinv.setCurrentIndex(2)
        QtCore.QMetaObject.connectSlotsByName(WidgetInventory)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../content/image/icon/poke-ball.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_pokeball, icon, "")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../content/image/icon/close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pushButton_close.setIcon(icon1)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../content/image/icon/potion.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_potion, icon2, "")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../content/image/icon/rare.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_rare, icon3, "")

    def retranslateUi(self, WidgetInventory):
        _translate = QtCore.QCoreApplication.translate
        WidgetInventory.setWindowTitle(_translate("WidgetInventory", "Form"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    WidgetInventory = QtWidgets.QWidget()
    ui = Ui_WidgetInventory()
    ui.setupUi(WidgetInventory)
    WidgetInventory.show()
    sys.exit(app.exec_())
