# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'content/interface/ui/inventory/FormInventory.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_FrameInventory(object):
    def setupUi(self, FrameInventory):
        FrameInventory.setObjectName("FrameInventory")
        FrameInventory.resize(121, 88)
        self.verticalLayout = QtWidgets.QVBoxLayout(FrameInventory)
        self.verticalLayout.setObjectName("verticalLayout")
        self.inv_name = QtWidgets.QLabel(FrameInventory)
        self.inv_name.setText("")
        self.inv_name.setAlignment(QtCore.Qt.AlignCenter)
        self.inv_name.setObjectName("inv_name")
        self.verticalLayout.addWidget(self.inv_name)
        self.pushButton_remove = QtWidgets.QPushButton(FrameInventory)
        self.pushButton_remove.setObjectName("pushButton_remove")
        self.verticalLayout.addWidget(self.pushButton_remove)

        self.retranslateUi(FrameInventory)
        QtCore.QMetaObject.connectSlotsByName(FrameInventory)

    def retranslateUi(self, FrameInventory):
        _translate = QtCore.QCoreApplication.translate
        FrameInventory.setWindowTitle(_translate("FrameInventory", "Frame"))
        self.pushButton_remove.setText(_translate("FrameInventory", "PushButton"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    FrameInventory = QtWidgets.QFrame()
    ui = Ui_FrameInventory()
    ui.setupUi(FrameInventory)
    FrameInventory.show()
    sys.exit(app.exec_())
