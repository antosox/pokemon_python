# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'content/interface/ui/inventory.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_WidgetInventory(object):
    def setupUi(self, WidgetInventory):
        WidgetInventory.setObjectName("WidgetInventory")
        WidgetInventory.resize(500, 600)
        WidgetInventory.setAutoFillBackground(False)
        WidgetInventory.setStyleSheet("")

        self.verticalLayout_3 = QtWidgets.QVBoxLayout(WidgetInventory)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.widget = QtWidgets.QWidget(WidgetInventory)
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName("horizontalLayout")

        self.qtabinv = QtWidgets.QTabWidget(self.widget)
        self.qtabinv.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.qtabinv.setObjectName("qtabinv")

        self.tab_pokeball = QtWidgets.QWidget()
        self.tab_pokeball.setAccessibleName("")
        self.tab_pokeball.setObjectName("tab_pokeball")

        self.verticalLayout = QtWidgets.QVBoxLayout(self.tab_pokeball)
        self.verticalLayout.setObjectName("verticalLayout")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../content/image/icon/pokeball.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.qtabinv.addTab(self.tab_pokeball, icon, "")
        self.tab_potion = QtWidgets.QWidget()
        self.tab_potion.setObjectName("tab_potion")

        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_potion)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.widget_3 = QtWidgets.QWidget(self.tab_potion)
        self.widget_3.setObjectName("widget_3")

        # self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widget_3)
        # self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        # self.frame = QtWidgets.QFrame(self.widget_3)
        # self.frame.setMaximumSize(QtCore.QSize(100, 100))
        # self.frame.setBaseSize(QtCore.QSize(0, 0))
        # self.frame.setStyleSheet("")
        # self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        # self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        # self.frame.setObjectName("frame")
        #
        # self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.frame)
        # self.verticalLayout_4.setObjectName("verticalLayout_4")
        # self.name = QtWidgets.QLabel(self.frame)
        # self.name.setStyleSheet("")
        # self.name.setTextFormat(QtCore.Qt.AutoText)
        # self.name.setAlignment(QtCore.Qt.AlignCenter)
        # self.name.setObjectName("name")
        # self.verticalLayout_4.addWidget(self.name)
        # self.pushButton = QtWidgets.QPushButton(self.frame)
        # self.pushButton.setStyleSheet("")
        # self.pushButton.setText("")
        # icon1 = QtGui.QIcon()
        # icon1.addPixmap(QtGui.QPixmap("../content/image/icon/close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.pushButton.setIcon(icon1)
        # self.pushButton.setObjectName("pushButton")
        # self.verticalLayout_4.addWidget(self.pushButton)
        # self.horizontalLayout_3.addWidget(self.frame)
        # spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Minimum)
        # self.horizontalLayout_3.addItem(spacerItem)
        # self.verticalLayout_2.addWidget(self.widget_3)
        # spacerItem1 = QtWidgets.QSpacerItem(20, 250, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.MinimumExpanding)
        # self.verticalLayout_2.addItem(spacerItem1)
        # icon2 = QtGui.QIcon()
        # icon2.addPixmap(QtGui.QPixmap("../content/image/icon/potion.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.qtabinv.addTab(self.tab_potion, icon2, "")
        # self.tab_rare = QtWidgets.QWidget()
        # self.tab_rare.setObjectName("tab_rare")
        # icon3 = QtGui.QIcon()
        # icon3.addPixmap(QtGui.QPixmap("../content/image/icon/rare.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.qtabinv.addTab(self.tab_rare, icon3, "")
        # self.horizontalLayout.addWidget(self.qtabinv)
        # self.verticalLayout_3.addWidget(self.widget)
        # self.widget_2 = QtWidgets.QWidget(WidgetInventory)
        # self.widget_2.setObjectName("widget_2")
        # self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget_2)
        # self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        # spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        # self.horizontalLayout_2.addItem(spacerItem2)
        # self.pushButton_close = QtWidgets.QPushButton(self.widget_2)
        # self.pushButton_close.setText("")
        # self.pushButton_close.setIcon(icon1)
        # self.pushButton_close.setIconSize(QtCore.QSize(80, 20))
        # self.pushButton_close.setObjectName("pushButton_close")
        # self.horizontalLayout_2.addWidget(self.pushButton_close)
        # self.verticalLayout_3.addWidget(self.widget_2)

        self.retranslateUi(WidgetInventory)
        self.qtabinv.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(WidgetInventory)

    def retranslateUi(self, WidgetInventory):
        _translate = QtCore.QCoreApplication.translate
        WidgetInventory.setWindowTitle(_translate("WidgetInventory", "Form"))
        self.name.setText(_translate("WidgetInventory", "TextLabel"))
