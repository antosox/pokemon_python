# !/usr/bin/python37
# -*- coding: utf-8 -*-

import json

from model.Player import Player


class Inventory:

    def __init__(self, name=None):

        if name is None:
            pass
        else:
            self.objectName = name
            self.player = Player("Sasha")

    def majInventory(self, itemName=None, itemType=None, qte=None):
        with open('../save/player.json', "r+") as json_data:
            data_dict = json.load(json_data)
            # print(itemName)
            # print(itemType)
            # print(qte)
            data_dict['inventory'][0][itemName][0][itemType] += qte
            json_data.seek(0)
            json_data.truncate()
            json_data.seek(0)
            json.dump(data_dict, json_data)

    def showInventory(self):
        with open('../save/player.json', 'r+') as json_data:
            data = json.load(json_data)
            return data['inventory']