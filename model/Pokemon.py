import json
import random

import requests
from model import Move
class Pokemon:

    def __init__(self, id=None, name=None, types=None, moves=None, stats=None, frontDefault=None, backDefault=None):

        if id is None:
            pass
        else:
            self.id = id
            self.name = name
            self.types = types
            self.moves = moves
            self.stats = stats;
            self.frontDefault = frontDefault
            self.backDefault = backDefault

    def getId(self):
        return self.id

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getTypes(self):
        return self.types

    def setTypes(self, types):
        self.types = types

    def getMoves(self):
        return self.moves

    def setMoves(self, moves):
        self.moves = moves

    def getStats(self):
        return self.stats

    def setStats(self, stats):
        self.stats = stats

    def getBackDefault(self):
        return self.backDefault

    def setBackDefault(self, backDefault):
        self.backDefault = backDefault

    def getFrontDefault(self):
        return self.frontDefault

    def setFrontDefault(self, frontDefault):
        self.frontDefault = frontDefault

    def getDetailsMoves(self,moves):
        tableau_moves = {}
        for k in range(len(moves[0])):
            tableau_move = {}
            url = moves[0][k]['url']
            request = requests.get(url)
            name = request.json()['name']
            power = request.json()['power']
            type = request.json()['type']['name']
            tableau_move["name"] = name
            tableau_move["power"] = power
            tableau_move["type"] = type
            tableau_moves[k] = tableau_move
        return tableau_moves

    def Update_Xp(self, id, nivPokeAdvers):
        with open('../save/player.json','r+') as json_data_player:
            data_player = json.load(json_data_player)
            name_poke = data_player["pokemons"][0][str(id)]["name"]
            level_avant = data_player["pokemons"][0][str(id)]["Stats"]["level"]
            request = requests.get("https://pokeapi.co/api/v2/pokemon/" + name_poke)
            base_xp = request.json()['base_experience']
            name = request.json()["forms"][0]['name']
            xpWin = int(base_xp*(nivPokeAdvers/7))
            # xpWin = 2535
            data_player["pokemons"][0][str(id)]["Stats"]["xp"] += xpWin
            xp_actuel = data_player["pokemons"][0][str(id)]["Stats"]["xp"]
            species = request.json()["species"]['url']
            request2 = requests.get(species)
            evolution_chain = request2.json()["evolution_chain"]["url"]
            growth_rate = request2.json()["growth_rate"]["url"]
            request3 = requests.get(evolution_chain)
            if name == request3.json()["chain"]["species"]["name"] :
                evolves_to = request3.json()["chain"]["evolves_to"][0]["species"]["name"]
                min_level = request3.json()["chain"]["evolves_to"][0]["evolution_details"][0]["min_level"]
            elif name == request3.json()["chain"]["evolves_to"][0]["species"]["name"]:
                evolves_to = request3.json()["chain"]["evolves_to"][0]["evolves_to"][0]["species"]["name"]
                min_level = request3.json()["chain"]["evolves_to"][0]["evolves_to"][0]["evolution_details"][0]["min_level"]
            else :
                evolves_to = None
                min_level = None
            request4 = requests.get(growth_rate)
            i = 99
            tab = request4.json()["levels"]
            # [i]["experience"]
            while xp_actuel >= request4.json()["levels"][i]["experience"]:
                level = request4.json()["levels"][i]["level"]
                i-=1

            if min_level is None:
                data_player["pokemons"][0][str(id)]["Stats"]["level"] = level
                data_player["pokemons"][0][str(id)]["Stats"]["xp"] = xp_actuel
                json_data_player.seek(0)
                json_data_player.truncate()
                json_data_player.seek(0)
                json.dump(data_player, json_data_player)
            else :
                if level >= min_level:
                    if level_avant < min_level:
                        with open('../save/pokedex.json', 'r+') as json_data:
                            data = json.load(json_data)
                            k = 1
                            while evolves_to != data[str(k)]["name"]:
                                k+=1
                            i = 0
                            moves = {}
                            tableau_random = []
                            # while i < 4:
                            #     num = random.randint(0, len(data[str(k)]["moves"]) - 1)
                            #     while tableau_random.__contains__(num):
                            #         num = random.randrange(0, 1, len(data[str(k)]["moves"]))
                            #     tableau_random.append(num)
                            #     move = data[str(k)]["moves"][str(num)]
                            #     moves[i] = move
                            #     i += 1
                            data[str(k)]["moves"] = data_player["pokemons"][0][str(id)]["moves"]
                            # data[str(k)]["stats"]["niveau"] = min_level
                            with open('../save/player.json', "r+") as json_data_player:
                                data_player = json.load(json_data_player)
                                dico = {}
                                dico[str(id)] = data[str(k)]
                                data_player["pokemons"][0][str(id)] = dico[str(id)]
                                data_player["pokemons"][0][str(id)]["Stats"]["level"] = level
                                data_player["pokemons"][0][str(id)]["Stats"]["xp"] = xp_actuel
                                json_data_player.seek(0)
                                json_data_player.truncate()
                                json_data_player.seek(0)
                                json.dump(data_player, json_data_player)
                else :
                    data_player["pokemons"][0][str(id)]["Stats"]["level"] = level
                    data_player["pokemons"][0][str(id)]["Stats"]["xp"] = xp_actuel
                    json_data_player.seek(0)
                    json_data_player.truncate()
                    json_data_player.seek(0)
                    json.dump(data_player, json_data_player)

    def addPokemon(self, id,niv):
        request = requests.get("https://pokeapi.co/api/v2/pokemon/" + str(id))
        base_xp = request.json()['base_experience']
        name = request.json()["forms"][0]['name']
        species = request.json()["species"]['url']
        request2 = requests.get(species)
        evolution_chain = request2.json()["evolution_chain"]["url"]
        request3 = requests.get(evolution_chain)
        if name == request3.json()["chain"]["species"]["name"]:
            evolves_to = request3.json()["chain"]["evolves_to"][0]["species"]["name"]
            min_level = request3.json()["chain"]["evolves_to"][0]["evolution_details"][0]["min_level"]
            if min_level is None :
                niv = niv
            else:
                if niv > min_level:
                    niv = min_level - 1
        elif name == request3.json()["chain"]["evolves_to"][0]["species"]["name"]:
            evolves_to = request3.json()["chain"]["evolves_to"][0]["evolves_to"][0]["species"]["name"]
            min_level = request3.json()["chain"]["evolves_to"][0]["evolves_to"][0]["evolution_details"][0]["min_level"]
            if min_level is None :
                niv = niv
            else:
                if niv > min_level:
                    niv = min_level - 1
        else:
            min_level = request3.json()["chain"]["evolves_to"][0]["evolves_to"][0]["evolution_details"][0]["min_level"]
            if min_level is None :
                niv = niv
            else:
                niv = min_level+1

        with open('../save/pokedex.json', 'r+') as json_data:
            data = json.load(json_data)
            i = 0
            moves = {}
            tableau_random = []
            while i < 4:
                num = random.randint(0, len(data[str(id)]["moves"]) - 1)
                while tableau_random.__contains__(num):
                    num = random.randint(0, len(data[str(id)]["moves"]) - 1)
                tableau_random.append(num)
                move = data[str(id)]["moves"][str(num)]
                moves[i] = move
                i += 1
            data[str(id)]["moves"] = moves
            request = requests.get("https://pokeapi.co/api/v2/pokemon/" + str(id))
            species = request.json()["species"]['url']
            request2 = requests.get(species)
            growth_rate = request2.json()["growth_rate"]["url"]
            request4 = requests.get(growth_rate)
            i = 0
            # [i]["experience"]
            while niv != request4.json()["levels"][i]["level"]:
                i += 1
                xp = request4.json()["levels"][i]["experience"]
            data[str(id)]["Stats"]["xp"] = xp
            data[str(id)]["Stats"]["level"] = niv
            with open('../save/player.json', "r+") as json_data_player:
                data_player = json.load(json_data_player)
                dico = {}
                nb = data_player["nbpokemons"]
                dico[str(nb)] = data[str(id)]
                data_player["pokemons"][0][str(nb)] = dico[str(nb)]
                data_player["nbpokemons"] += 1
                json_data_player.seek(0)
                json_data_player.truncate()
                json_data_player.seek(0)
                json.dump(data_player, json_data_player)

    def showOneRand(self):
        with open('../save/pokedex.json', 'r+') as json_data:
            data = json.load(json_data)
            num = random.randint(0, len(data) - 1)

            pokemon = Pokemon(num, data[str(num)]['name'], data[str(num)]['types'], data[str(num)]['moves'],
                              data[str(num)]['Stats'], data[str(num)]['frontDefault'], data[str(num)]['backDefault'])
            return pokemon

    def showActivePokemons(self):
        with open('../save/player.json', 'r+') as json_data:
            data = json.load(json_data)

            i = 0
            arrPokemon = []
            while i < len(data['pokemons'][0]) and i <= 5:
                pokemon = Pokemon(i, data['pokemons'][0][str(i)]['name'], data['pokemons'][0][str(i)]['types'],
                                  data['pokemons'][0][str(i)]['moves'], data['pokemons'][0][str(i)]['Stats'],
                                  data['pokemons'][0][str(i)]['frontDefault'], data['pokemons'][0][str(i)]['backDefault'])
                arrPokemon.append(pokemon)
                i+=1

            return arrPokemon