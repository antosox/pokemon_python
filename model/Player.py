# !/usr/bin/python37
# -*- coding: utf-8 -*-

import json

class Player:

    def __init__(self, name=None):
        if name is None:
            self.filePath = "../save/player.json"
        else:
            self.name = name
            self.inventory = None
            self.filePath = "../save/player.json"

    def getInventoryPlayer(self):
        with open(self.filePath, "r+") as json_data:
            data_dict = json.load(json_data)
            return data_dict

    def getSolde(self):
        with open(self.filePath, "r+") as json_data:
            data_dict = json.load(json_data)
            solde = data_dict['solde']
        return solde

    def updateSolde(self,solde):
        with open(self.filePath, "r+") as json_data:
            data_dict = json.load(json_data)
            data_dict['solde'] = solde
            json_data.seek(0)
            json_data.truncate()
            json_data.seek(0)
            json.dump(data_dict, json_data)

    def getFilePathJson(self):
        return self.filePath

    def getName(self):
        return self.name
