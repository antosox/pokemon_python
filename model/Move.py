# !/usr/bin/python37
# -*- coding: utf-8 -*-

import json

class Move:

    def __init__(self, name,damage,type):
        self.name = name
        self.damage = damage
        self.type = type


    def getName(self):
        return self.name

    def setName(self,name) :
        self.name = name

    def getDamage(self):
        return self.damage

    def setDamage(self, damage):
        self.damage = damage

    def getType(self):
        return self.type

    def setType(self, type):
        self.type = type