import json
from urllib import request

import requests

from model.Pokemon import Pokemon


class Pokedex:

    def __init__(self):
        self.Initialize()

    def Initialize(self):
        self.pokemons = []
        i = 0
        while i < 3:
            types = {}
            moves = []
            tab1 = []
            stats = {}
            i = i + 1
            request = requests.get("https://pokeapi.co/api/v2/pokemon/" + str(i))

            name = request.json()["forms"][0]['name']

            spritFront = request.json()["sprites"]['front_default']
            spritBack = request.json()["sprites"]['back_default']

            for k in range(len(request.json()["types"])):
                if request.json()["types"][k]["slot"] == 1:
                    types["type"] = request.json()["types"][k]['type']['name']
                    # types.append(request.json()["types"][k]['type']['name'])
                    break

            # types.append(request.json()["types"][0]['type']['name'])
            # for requested in (request.json()["types"]):
            #     types.append(requested['type']['name'])
            for move in range(len(request.json()['moves'])) :
                tab1.append(request.json()['moves'][move-1]['move'])
            moves.append(tab1)

            for v in range(len(request.json()['stats'])):
                stat = {}
                if request.json()['stats'][v]['stat']['name'] == "speed" or request.json()['stats'][v]['stat'][
                    'name'] == "defense" or request.json()['stats'][v]['stat']['name'] == "attack" or \
                        request.json()['stats'][v]['stat']['name'] == "hp":
                    name_stat = request.json()['stats'][v]['stat']['name']
                    base_stat = request.json()['stats'][v]['base_stat']
                    stats[name_stat] = base_stat
                if name_stat == "hp":
                    stat = {}
                    stats["hpactuel"] = base_stat
            pokemon = Pokemon(i, name, types, moves,stats, spritFront, spritBack)
            self.pokemons.append(pokemon)

    def showAll(self):
        return self.pokemons

    def showOne(self, id):
        myPokemon = None
        for pokemon in self.pokemons:
            if pokemon.getId() == id:
                myPokemon = pokemon
        return myPokemon

