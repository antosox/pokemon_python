from model.Inventory import Inventory
class Trader:

    def __init__(self, name, inventory):
        self.nom = name
        self.inventory = Inventory(name)

    def getId(self):
        return self.id

    def getNom(self):
        return self.name

    def setNom(self, name):
        self.nom = name

    def getInventaire(self):
        return self.inventory

    def setInventaire(self, inventory):
        self.inventory = inventory

