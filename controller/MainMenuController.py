from PyQt5.QtCore import QByteArray
from PyQt5.QtGui import QMovie
from PyQt5.QtWidgets import QWidget, QApplication, QMainWindow, QDialog, QLabel
import sys
from controller.SaveController import Save
from content.interface.py.MainMenu import Ui_MainWindow
from controller.MpConnectionController import MpConnectionDialog
from controller import Map
from PyQt5 import QtGui

class MainMenuWidget(QMainWindow):

    def resizeEvent(self, a0: QtGui.QResizeEvent):

        self.pixmap = self.pixmap1.scaled(self.width(), self.height())
        self.label.setPixmap(self.pixmap)
        self.label.resize(self.width(), self.height())

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.pixmap1 = QtGui.QPixmap('./../content/menu/bck.png')
        self.label = QLabel(self)
        self.resizeEvent(self.pixmap1)
        self.show()
        self.move(200,30)
        self.resize(1100, 1000)
        self.setFixedSize(1100, 1000)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton_3.clicked.connect(self.callDialogMp)
        self.ui.pushButton.clicked.connect(self.startGame)
        self.ui.pushButton_2.clicked.connect(self.loadGame)

    def startGame(self):
        save=Save()
        save.restart()
        self.mp= Map.MyWidget()
        self.hide()

    def callDialogMp(self):
        mp = MpConnectionDialog()

    def loadGame(self):
        self.mp = Map.MyWidget()
        self.hide()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    mainwindow = MainMenuWidget()
    mainwindow.show()

    sys.exit(app.exec_())
