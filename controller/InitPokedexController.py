import json

import sys

from PyQt5.QtWidgets import QApplication
from model import Pokedex

if __name__ == "__main__":
    app = QApplication(sys.argv)

    with open('../save/pokedex.json', "w") as json_data:
        #data = json.load(json_data)

        pokedex = Pokedex.Pokedex()
        pokemons = pokedex.showAll()
        dict = {}
        for pokemon in pokemons:
            tabtest = {}
            tabtest["name"] = pokemon.name
            tabtest["types"] = pokemon.types
            details = pokemon.getDetailsMoves(pokemon.moves)
            pokemon.setMoves(details)
            tabtest["moves"] = pokemon.moves
            tabtest["Stats"] = pokemon.stats
            tabtest["backDefault"] = pokemon.backDefault
            tabtest["frontDefault"] = pokemon.frontDefault
            dict[pokemon.id] = tabtest
        json.dump(dict,json_data)
        # json_data.seek(0)
        # json_data.truncate()
        # json_data.seek(0)
        #json.dump(data, json_data)
    # print(data)
    sys.exit(app.exec_())