# !/usr/bin/python37
# -*- coding: utf-8 -*-
import random
import winsound


import urllib.request, time

from controller.SocketWrapper import SocketWrapper
from model.Pokemon import Pokemon
from model.Inventory import Inventory
from model.Player import Player
from content.interface.py.Combat import Ui_Form

from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPixmap

class CombatWidget(QWidget):

    def __init__(self, multiplayer=False, parent=None):
        QWidget.__init__(self, parent=parent)

        self.pokemon = Pokemon()
        self.inventory = Inventory()
        self.player = Player("Sacha")
        self.winner = False

        self.multiplayer = multiplayer;

        if multiplayer:
            self.socketWraper = SocketWrapper()

        # Chargement Pokemons
        self.listePokemon = self.pokemon.showActivePokemons()

        # Chargement Pokemon Adverse
        self.pokemon_adverse = self.pokemon.showOneRand()

        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.setWindowTitle("Combat")
        self.ui.widget_abilities.hide()
        self.ui.widget_pokemons.hide()
        self.ui.widget_sac.hide()

        # -------- CSS --------
        # Menu
        self.ui.widget_menu.setObjectName("widget_menu")
        self.ui.widget_menu.setStyleSheet("#widget_menu { background-color: white }")

        self.ui.pushButton_Attaque.setStyleSheet("height: 100px; background-color: #995152; border: 2px solid black")
        self.ui.pushButton_Sac.setStyleSheet(
            "height: 50px; width:200px; background-color: #806828; border: 2px solid black")
        self.ui.pushButton_Fuite.setStyleSheet(
            "height: 50px; width:200px; background-color: #285070; border: 2px solid black")
        self.ui.pushButton_Pokemon.setStyleSheet(
            "height: 50px; width:200px; background-color: #3b6625; border: 2px solid black")

        self.ui.widget_frontName.setObjectName("widget_frontName")
        self.ui.widget_frontName.setStyleSheet("#widget_frontName { background-color: white; border: 2px solid black; }")

        self.ui.label_backInfoNom.setStyleSheet("font-weight: 700")

        self.ui.widget_backName.setObjectName("widget_backName")
        self.ui.widget_backName.setStyleSheet("#widget_backName { background-color: white; border: 2px solid black; }")

        self.ui.label_frontInfoNom.setStyleSheet("font-weight: 700")

        self.ui.widget_info.setObjectName("widget_info")
        self.ui.widget_info.setStyleSheet("#widget_info { background-color: white; border: 2px solid black; border-radius: 5px; width: 100%; text-align: center }")

        link = "../content/image/bg-battle.png"
        self.ui.widget_screenTop.setObjectName("widget_screenTop")
        self.ui.widget_screenTop.setStyleSheet(
            "#widget_screenTop { background-image: url(" + link + "); background-repeat: no-repeat; background-size: 100%; }")

        # Sac
        self.ui.widget_sac.setObjectName("widget_sac")
        self.ui.widget_sac.setStyleSheet("#widget_sac { background-color: white }")

        self.ui.pushButton_objet1.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-left: 30px")
        self.ui.pushButton_objet2.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-right: 30px")
        self.ui.pushButton_objet3.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-left: 30px")
        self.ui.pushButton_objet4.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-right: 30px")
        self.ui.pushButton_objet5.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-left: 30px")
        self.ui.pushButton_objet6.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-right: 30px")
        self.ui.pushButton_objet7.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-left: 30px")
        self.ui.pushButton_objet8.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab; margin-right: 30px")

        self.ui.pushButton_cancel3.setStyleSheet("height: 50px; background-color: #24445d; border: 2px solid black")

        # Attaque
        self.ui.widget_abilities.setObjectName("widget_abilities")
        self.ui.widget_abilities.setStyleSheet("#widget_abilities { background-color: white }")

        self.ui.pushButton_cancel.setStyleSheet("height: 50px; background-color: #24445d; border: 2px solid black")

        # Pokemon
        self.ui.widget_pokemons.setObjectName("widget_pokemons")
        self.ui.widget_pokemons.setStyleSheet("#widget_pokemons { background-color: white }")

        self.ui.pushButton_pokemon1.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab")
        self.ui.pushButton_pokemon2.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab")
        self.ui.pushButton_pokemon3.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab")
        self.ui.pushButton_pokemon4.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab")
        self.ui.pushButton_pokemon5.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab")
        self.ui.pushButton_pokemon6.setStyleSheet(
            "height: 30px; width: 200px; border: 2px solid black; background-color: #f1d6ab")

        self.ui.pushButton_cancel2.setStyleSheet("height: 50px; background-color: #24445d; border: 2px solid black")

        # -------- Buttons --------
        # Menu
        self.ui.pushButton_Attaque.clicked.connect(self.Attaque)
        self.ui.pushButton_Sac.clicked.connect(self.Sac)
        self.ui.pushButton_Fuite.clicked.connect(self.Fuite)
        self.ui.pushButton_Pokemon.clicked.connect(self.Pokemon)
        self.ui.label_information.setText("Le combat commence !")

        # Sac
        self.ui.pushButton_objet1.clicked.connect(lambda: self.ActionObjet(0))
        self.ui.pushButton_objet2.clicked.connect(lambda: self.ActionObjet(1))
        self.ui.pushButton_objet3.clicked.connect(lambda: self.ActionObjet(2))
        self.ui.pushButton_objet4.clicked.connect(lambda: self.ActionObjet(3))
        self.ui.pushButton_objet5.clicked.connect(lambda: self.ActionObjet(4))
        self.ui.pushButton_objet6.clicked.connect(lambda: self.ActionObjet(5))
        self.ui.pushButton_objet7.clicked.connect(lambda: self.ActionObjet(6))
        self.ui.pushButton_objet8.clicked.connect(lambda: self.ActionObjet(7))
        self.ui.pushButton_cancel3.clicked.connect(self.Menu)

        # Attaque
        self.ui.pushButton_ability1.clicked.connect(lambda: self.VitesseAttaque(0))
        self.ui.pushButton_ability2.clicked.connect(lambda: self.VitesseAttaque(1))
        self.ui.pushButton_ability3.clicked.connect(lambda: self.VitesseAttaque(2))
        self.ui.pushButton_ability4.clicked.connect(lambda: self.VitesseAttaque(3))
        self.ui.pushButton_cancel.clicked.connect(self.Menu)

        # Pokemon
        self.ui.pushButton_pokemon1.clicked.connect(lambda: self.ActionSwitchPokemon(0))
        self.ui.pushButton_pokemon2.clicked.connect(lambda: self.ActionSwitchPokemon(1))
        self.ui.pushButton_pokemon3.clicked.connect(lambda: self.ActionSwitchPokemon(2))
        self.ui.pushButton_pokemon4.clicked.connect(lambda: self.ActionSwitchPokemon(3))
        self.ui.pushButton_pokemon5.clicked.connect(lambda: self.ActionSwitchPokemon(4))
        self.ui.pushButton_pokemon6.clicked.connect(lambda: self.ActionSwitchPokemon(5))
        self.ui.pushButton_cancel2.clicked.connect(self.Menu)
        winsound.PlaySound("../content/musique/PkmRS_Battle1.wav", winsound.SND_ASYNC )

        self.loadInformation()
        self.show()

    def EndCombat(self):

        if self.winner == True :
            winsound.PlaySound("../content/musique/PkmRS_Victory4.wav",winsound.SND_ALIAS)
            nivList = [];
            for i in self.listePokemon:
                nivList.append(i.getStats()["level"])

            nivAdverse = (sum(nivList)/len(nivList)) + (random.randint(0, 10) - 5)

            if nivAdverse <= 0:
                nivAdverse = 1

            self.pokemon.Update_Xp(self.listePokemon[0].getId(), nivAdverse);

            argentGagnée = int(20 + 5 * nivAdverse)

            self.player.updateSolde(self.player.getSolde() + argentGagnée)

            self.ui.label_information.setText("Vous avez gagner " + str(argentGagnée) + " Pokedollars")
            self.ui.label_information.repaint()
            time.sleep(1)

        self.close()

    def Attaque(self):
        self.ui.widget_menu.hide()
        self.ui.widget_abilities.show()

    def Sac(self):
        self.ui.widget_menu.hide()
        self.ui.widget_sac.show()

    def Fuite(self):
        self.close()

    def Pokemon(self):
        self.ui.widget_menu.hide()
        self.ui.widget_pokemons.show()

    def Menu(self):

        self.PutButtonsOn()

        self.ui.widget_abilities.hide()
        self.ui.widget_pokemons.hide()
        self.ui.widget_sac.hide()
        self.ui.widget_menu.show()

    def VitesseAttaque(self, n_attaque):

        self.PutButtonsOff()

        if self.listePokemon[0].getStats()["speed"] > self.pokemon_adverse.getStats()["speed"]:
            self.ui.label_information.setText(self.listePokemon[0].getName().capitalize() + " est plus rapide que " + self.pokemon_adverse.getName().capitalize())
            self.ui.label_information.repaint()
            time.sleep(1)

            self.ActionAttaquer(n_attaque)
            self.ActionDefendre()
        elif self.listePokemon[0].getStats()["speed"] < self.pokemon_adverse.getStats()["speed"]:
            self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " est plus rapide que " + self.listePokemon[0].getName().capitalize())
            self.ui.label_information.repaint()
            time.sleep(1)

            self.ActionDefendre()
            self.ActionAttaquer(n_attaque)
        else:
            num = random.randint(1, 2)
            if num == 1:
                self.ui.label_information.setText(self.listePokemon[0].getName().capitalize() + " est plus rapide que " + self.pokemon_adverse.getName().capitalize())
                self.ui.label_information.repaint()
                time.sleep(1)

                self.ActionAttaquer(n_attaque)
                self.ActionDefendre()
            else:
                self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " est plus rapide que " + self.listePokemon[0].getName().capitalize())
                self.ui.label_information.repaint()
                time.sleep(1)

                self.ActionDefendre()
                self.ActionAttaquer(n_attaque)

        self.Menu()

    def ActionAttaquer(self, n_attaque):

        self.ui.label_information.setText(self.listePokemon[0].getName().capitalize() + " utilise l'attaque " + self.listePokemon[0].getMoves()[str(n_attaque)]["name"])
        self.ui.label_information.repaint()
        time.sleep(1)

        if self.listePokemon[0].getMoves()[str(n_attaque)]["power"] is None:

            tmp = self.listePokemon[0].getStats()
            tmp["defense"] = tmp["defense"] + 15
            self.listePokemon[0].setStats(tmp)
            self.ui.label_information.setText("La défense de " + self.listePokemon[0].getName().capitalize() + " à été augmenté")
            self.ui.label_information.repaint()
            time.sleep(1)
        else:
            degats = 14 * self.listePokemon[0].getMoves()[str(n_attaque)]["power"] / self.pokemon_adverse.getStats()["defense"]
            self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " perd " + str(round(degats)) + " HP")
            self.ui.label_information.repaint()
            time.sleep(1)
            if self.pokemon_adverse.getStats()["hpactuel"] - degats <= 0:
                self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " est mort")
                self.ui.label_information.repaint()
                time.sleep(1)
                self.winner = True;
                self.EndCombat()
            else:
                tmp = self.pokemon_adverse.getStats()
                tmp["hpactuel"] = tmp["hpactuel"] - round(degats)
                self.pokemon_adverse.setStats(tmp)

        self.loadInformation()

    def ActionDefendre(self):

        n_attaque = random.randint(0, len(self.pokemon_adverse.getMoves()) - 1);

        self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " utilise l'attaque " + self.pokemon_adverse.getMoves()[str(n_attaque)]["name"])
        self.ui.label_information.repaint()
        time.sleep(1)

        if self.pokemon_adverse.getMoves()[str(n_attaque)]["power"] is None:

            tmp = self.pokemon_adverse.getStats()
            tmp["defense"] = tmp["defense"] + 15
            self.pokemon_adverse.setStats(tmp)
            self.ui.label_information.setText("La défense de " + self.pokemon_adverse.getName().capitalize() + " à été augmenté")
            self.ui.label_information.repaint()
            time.sleep(1)
        else:
            degats = 14 * self.pokemon_adverse.getMoves()[str(n_attaque)]["power"] / self.listePokemon[0].getStats()["defense"]
            self.ui.label_information.setText(self.listePokemon[0].getName().capitalize() + " perd " + str(round(degats)) + " HP")
            self.ui.label_information.repaint()
            time.sleep(1)

            if self.listePokemon[0].getStats()["hpactuel"] - round(degats) <= 0:
                self.ui.label_information.setText(self.listePokemon[0].getName().capitalize() + " est mort")
                self.ui.label_information.repaint()
                time.sleep(1)
                self.EndCombat()
            else:
                tmp = self.listePokemon[0].getStats()
                tmp["hpactuel"] = tmp["hpactuel"] - round(degats)
                self.listePokemon[0].setStats(tmp)

        self.loadInformation()

    def ActionObjet(self, n_objet):

        self.PutButtonsOff()

        inventaire = self.inventory.showInventory()

        used = False

        if n_objet == 0 and inventaire[0]['pokeballs'][0]['PokeBall'] > 0:
            self.ui.label_information.setText("Une Poké Ball est utilisé")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Capture(1)
            self.inventory.majInventory('pokeballs', 'PokeBall', -1)
            used = True

        elif n_objet == 1 and inventaire[0]['pokeballs'][0]['SuperBall'] > 0:
            self.ui.label_information.setText("Une Super Ball est utilisé")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Capture(1.5)
            self.inventory.majInventory('pokeballs', 'SuperBall', -1)
            used = True

        elif n_objet == 2 and inventaire[0]['pokeballs'][0]['HyperBall'] > 0:
            self.ui.label_information.setText("Une Hyper Ball est utilisé")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Capture(2)
            self.inventory.majInventory('pokeballs', 'HyperBall', -1)
            used = True

        elif n_objet == 3 and inventaire[0]['pokeballs'][0]['MasterBall'] > 0:
            self.ui.label_information.setText("Une Master Ball est utilisé")
            self.Capture(255)
            self.inventory.majInventory('pokeballs', 'MasterBall', -1)
            used = True

        elif n_objet == 4 and inventaire[0]['potions'][0]['Potion'] > 0:
            self.ui.label_information.setText("Une Potion est utilisé, " + self.listePokemon[0].getName().capitalize() + "regagne 20 HP")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Heal(20)
            self.inventory.majInventory('potions', 'Potion', -1)
            used = True

        elif n_objet == 5 and inventaire[0]['potions'][0]['Super Potion'] > 0:
            self.ui.label_information.setText("Une Super Potion est utilisé, " + self.listePokemon[0].getName().capitalize() + "regagne 50 HP")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Heal(50)
            self.inventory.majInventory('potions', 'Super Potion', -1)
            used = True

        elif n_objet == 6 and inventaire[0]['potions'][0]['Hyper Potion'] > 0:
            self.ui.label_information.setText("Une Hyper Potion est utilisé, " + self.listePokemon[0].getName().capitalize() + "regagne 200 HP")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Heal(200)
            self.inventory.majInventory('potions', 'Hyper Potion', -1)
            used = True

        elif n_objet == 7 and inventaire[0]['potions'][0]['Potion Max'] > 0:
            self.ui.label_information.setText("Une Potion Max est utilisé, " + self.listePokemon[0].getName().capitalize() + "regagne tout ses HP")
            self.ui.label_information.repaint()
            time.sleep(1)
            self.Heal(2500)
            self.inventory.majInventory('potions', 'Potion Max', -1)
            used = True

        if used == True:
            self.loadInformation()
            self.ActionDefendre()
            self.Menu()

    def Heal(self, value):

        tmp = self.listePokemon[0].getStats()
        tmp["hpactuel"] = tmp["hpactuel"] + value
        self.listePokemon[0].setStats(tmp)

        if self.listePokemon[0].getStats()["hpactuel"] > self.listePokemon[0].getStats()["hp"]:
            tmp = self.listePokemon[0].getStats()
            tmp["hpactuel"] = tmp["hp"]
            self.listePokemon[0].setStats(tmp)

    def Capture(self, value):
        a = ((1 - (2 / 3) * (self.pokemon_adverse.getStats()["hpactuel"] / self.pokemon_adverse.getStats()["hp"])) * value*175 * 1 * 1.5)
        # print(str(a) + "test")
        if a > 255:
            #ATTRAPER
            nivList = [];
            for i in self.listePokemon:
                nivList.append(i.getStats()["level"])

            nivAdverse = (sum(nivList)/len(nivList)) + (random.randint(0, 10) - 5)

            if nivAdverse <= 0:
                nivAdverse = 1

            self.pokemon.addPokemon(self.pokemon_adverse.getId(), nivAdverse)
            self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " à été attrapé")
            self.ui.label_information.repaint()
            winsound.PlaySound("../content/musique/PkmRS_Victory4.wav", winsound.SND_ALIAS)
            time.sleep(1)
            self.EndCombat()
        else:
            c = 0.35;
            b = 65535 * c;
            nbr1 = random.randint(0, 65536 - 1);
            nbr2 = random.randint(0, 65536 - 1);
            nbr3 = random.randint(0, 65536 - 1);
            nbr4 = random.randint(0, 65536 - 1);

            if nbr1 <= b and nbr2 <= b and nbr3 <= b and nbr4 <= b:
                #ATTRAPER
                nivList = [];
                for i in self.listePokemon:
                    nivList.append(i.getStats()["level"])

                nivAdverse = (sum(nivList) / len(nivList)) + (random.randint(0, 10) - 5)

                if nivAdverse <= 0:
                    nivAdverse = 1

                self.pokemon.addPokemon(self.pokemon_adverse.getId(), nivAdverse)
                self.ui.label_information.setText(self.pokemon_adverse.getName().capitalize() + " à été attrapé")
                self.ui.label_information.repaint()
                time.sleep(1)
                winsound.PlaySound("../content/musique/PkmRS_Victory4.wav", winsound.SND_ALIAS)
                self.EndCombat()
            else:
                self.ui.label_information.setText("Echec de la capture")
                self.ui.label_information.repaint()
                time.sleep(1)

    def ActionSwitchPokemon(self, n_pokemon):
        self.PutButtonsOff()

        tmp = self.listePokemon[0]
        self.listePokemon[0] = self.listePokemon[n_pokemon]
        self.listePokemon[n_pokemon] = tmp

        self.ui.label_information.setText("Vous remplacer " + self.listePokemon[n_pokemon].getName().capitalize() + " par " + self.listePokemon[0].getName().capitalize())

        self.loadInformation()
        self.ui.label_information.repaint()
        time.sleep(1)
        self.ActionDefendre()
        self.Menu()

    def loadInformation(self):

        # -------- Initialisation --------
        # Menu
        self.loadImageURL(self.ui.label_backPicture, self.listePokemon[0].getBackDefault(), 3)
        self.loadImageURL(self.ui.label_frontPicture, self.pokemon_adverse.getFrontDefault(), 3)

        self.ui.label_backInfoNom.setText(self.listePokemon[0].getName().capitalize())
        self.ui.label_backInfoPV.setText(
            str(self.listePokemon[0].getStats()["hpactuel"]) + "/" + str(self.listePokemon[0].getStats()["hp"]) + " HP")
        self.ui.label_backInfoNIV.setText("Niveau : " + str(self.listePokemon[0].getStats()["level"]))

        self.ui.label_frontInfoNom.setText(self.pokemon_adverse.getName().capitalize())
        self.ui.label_frontInfoPV.setText(
            str(self.pokemon_adverse.getStats()["hpactuel"]) + "/" + str(self.pokemon_adverse.getStats()["hp"]) + " HP")

        # Sac
        self.loadImageFILE(self.ui.label_objet1, "..\content\image\icon\poke-ball.png", 0.3)
        self.loadImageFILE(self.ui.label_objet2, "..\content\image\icon\super-ball.png", 0.3)
        self.loadImageFILE(self.ui.label_objet3, "..\content\image\icon\hyper-ball.png", 0.3)
        self.loadImageFILE(self.ui.label_objet4, "..\content\image\icon\master-ball.png", 0.3)
        self.loadImageFILE(self.ui.label_objet5, "..\content\image\icon\potion.png", 0.2)
        self.loadImageFILE(self.ui.label_objet6, "..\content\image\icon\super-potion.png", 0.2)
        self.loadImageFILE(self.ui.label_objet7, "..\content\image\icon\hyper-potion.png", 0.2)
        self.loadImageFILE(self.ui.label_objet8, "..\content\image\icon\max-potion.png", 0.2)

        inventaire = self.inventory.showInventory()

        self.ui.pushButton_objet1.setText("Poké Ball - " + str(inventaire[0]['pokeballs'][0]['PokeBall']))
        self.ui.pushButton_objet2.setText("Super Ball - " + str(inventaire[0]['pokeballs'][0]['SuperBall']))
        self.ui.pushButton_objet3.setText("Hyper Ball - " + str(inventaire[0]['pokeballs'][0]['HyperBall']))
        self.ui.pushButton_objet4.setText("Master Ball - " + str(inventaire[0]['pokeballs'][0]['MasterBall']))
        self.ui.pushButton_objet5.setText("Potion - " + str(inventaire[0]['potions'][0]['Potion']))
        self.ui.pushButton_objet6.setText("Super Potion - " + str(inventaire[0]['potions'][0]['Super Potion']))
        self.ui.pushButton_objet7.setText("Hyper Potion - " + str(inventaire[0]['potions'][0]['Hyper Potion']))
        self.ui.pushButton_objet8.setText("Potion Max - " + str(inventaire[0]['potions'][0]['Potion Max']))

        # Attaque

        if self.listePokemon[0].getMoves()['0']['power'] is None:
            self.ui.pushButton_ability1.setText(self.listePokemon[0].getMoves()['0']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['0']['type'].capitalize())
        else:
            self.ui.pushButton_ability1.setText(self.listePokemon[0].getMoves()['0']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['0'][
                                                    'type'].capitalize() + "  -  " + str(
                self.listePokemon[0].getMoves()['0']['power']) + " ATK")

        if self.listePokemon[0].getMoves()['1']['power'] is None:
            self.ui.pushButton_ability2.setText(self.listePokemon[0].getMoves()['1']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['1']['type'].capitalize())
        else:
            self.ui.pushButton_ability2.setText(self.listePokemon[0].getMoves()['1']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['1'][
                                                    'type'].capitalize() + "  -  " + str(
                self.listePokemon[0].getMoves()['1']['power']) + " ATK")

        if self.listePokemon[0].getMoves()['2']['power'] is None:
            self.ui.pushButton_ability3.setText(self.listePokemon[0].getMoves()['2']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['2']['type'].capitalize())
        else:
            self.ui.pushButton_ability3.setText(self.listePokemon[0].getMoves()['2']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['2'][
                                                    'type'].capitalize() + "  -  " + str(
                self.listePokemon[0].getMoves()['2']['power']) + " ATK")

        if self.listePokemon[0].getMoves()['3']['power'] is None:
            self.ui.pushButton_ability4.setText(self.listePokemon[0].getMoves()['3']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['3']['type'].capitalize())
        else:
            self.ui.pushButton_ability4.setText(self.listePokemon[0].getMoves()['3']['name'].capitalize() + "\n\n" +
                                                self.listePokemon[0].getMoves()['3'][
                                                    'type'].capitalize() + "  -  " + str(
                self.listePokemon[0].getMoves()['3']['power']) + " ATK")

        self.ui.pushButton_ability1.setStyleSheet("border: 5px solid " + self.colorGenerator(
            self.listePokemon[0].getMoves()['0'][
                'type']) + "; background-color: #fff4e4; font-weight:600; height: 80px")
        self.ui.pushButton_ability2.setStyleSheet("border: 5px solid " + self.colorGenerator(
            self.listePokemon[0].getMoves()['1'][
                'type']) + "; background-color: #fff4e4; font-weight:600; height: 80px")
        self.ui.pushButton_ability3.setStyleSheet("border: 5px solid " + self.colorGenerator(
            self.listePokemon[0].getMoves()['2'][
                'type']) + "; background-color: #fff4e4; font-weight:600; height: 80px")
        self.ui.pushButton_ability4.setStyleSheet("border: 5px solid " + self.colorGenerator(
            self.listePokemon[0].getMoves()['3'][
                'type']) + "; background-color: #fff4e4; font-weight:600; height: 80px")

        # Pokemon
        self.loadImageURL(self.ui.label_pokemon1, self.listePokemon[0].getFrontDefault(), 1)
        self.ui.label_pokemon2.setText('') if (len(self.listePokemon) < 2) else self.loadImageURL(
            self.ui.label_pokemon2, self.listePokemon[1].getFrontDefault(), 1)
        self.ui.label_pokemon3.setText('') if (len(self.listePokemon) < 3) else self.loadImageURL(
            self.ui.label_pokemon3, self.listePokemon[2].getFrontDefault(), 1)
        self.ui.label_pokemon4.setText('') if (len(self.listePokemon) < 4) else self.loadImageURL(
            self.ui.label_pokemon4, self.listePokemon[3].getFrontDefault(), 1)
        self.ui.label_pokemon5.setText('') if (len(self.listePokemon) < 5) else self.loadImageURL(
            self.ui.label_pokemon5, self.listePokemon[4].getFrontDefault(), 1)
        self.ui.label_pokemon6.setText('') if (len(self.listePokemon) < 6) else self.loadImageURL(
            self.ui.label_pokemon6, self.listePokemon[5].getFrontDefault(), 1)

        self.ui.pushButton_pokemon1.setText(self.listePokemon[0].getName().capitalize())
        self.ui.pushButton_pokemon2.hide() if (len(self.listePokemon) < 2) else self.ui.pushButton_pokemon2.setText(
            self.listePokemon[1].getName().capitalize())
        self.ui.pushButton_pokemon3.hide() if (len(self.listePokemon) < 3) else self.ui.pushButton_pokemon3.setText(
            self.listePokemon[2].getName().capitalize())
        self.ui.pushButton_pokemon4.hide() if (len(self.listePokemon) < 4) else self.ui.pushButton_pokemon4.setText(
            self.listePokemon[3].getName().capitalize())
        self.ui.pushButton_pokemon5.hide() if (len(self.listePokemon) < 5) else self.ui.pushButton_pokemon5.setText(
            self.listePokemon[4].getName().capitalize())
        self.ui.pushButton_pokemon6.hide() if (len(self.listePokemon) < 6) else self.ui.pushButton_pokemon6.setText(
            self.listePokemon[5].getName().capitalize())

    def loadImageURL(self, item, url, size):
        data = urllib.request.urlopen(url).read()
        pixmap = QPixmap()
        pixmap.loadFromData(data)
        pixmap2 = pixmap.scaled(pixmap.width() * size, pixmap.height() * size)
        item.setPixmap(pixmap2)

    def loadImageFILE(self, item, file, size):
        pixmap = QPixmap()
        pixmap.load(file)
        pixmap2 = pixmap.scaled(pixmap.width() * size, pixmap.height() * size)
        item.setPixmap(pixmap2)

    def colorGenerator(self, type):
        arrColor = {
            "grass": "#439c25",
            "poison": "#783a77",
            "fire": "#ee4316",
            "flying": "#90a0f1",
            "water": "#3892e8",
            "bug": "#a8b826",
            "normal": "#bfbbaf",
            "electric": "#f1c134",
            "ground": "#cfb259",
            "fairy": "#f3bbf3",
            "fighting": "#81402b",
            "psychic": "#de608a",
            "rock": "#baa25f",
            "steel": "#cbccd1",
            "ice": "#b3eafb",
            "ghost": "#605cb2",
            "dragon": "#7865dc"}

        return arrColor[type]

    def PutButtonsOn(self):
        # Menu
        self.ui.pushButton_Attaque.setEnabled(True);
        self.ui.pushButton_Sac.setEnabled(True);
        self.ui.pushButton_Fuite.setEnabled(True);
        self.ui.pushButton_Pokemon.setEnabled(True);

        # Sac
        self.ui.pushButton_objet1.setEnabled(True);
        self.ui.pushButton_objet2.setEnabled(True);
        self.ui.pushButton_objet3.setEnabled(True);
        self.ui.pushButton_objet4.setEnabled(True);
        self.ui.pushButton_objet5.setEnabled(True);
        self.ui.pushButton_objet6.setEnabled(True);
        self.ui.pushButton_objet7.setEnabled(True);
        self.ui.pushButton_objet8.setEnabled(True);
        self.ui.pushButton_cancel3.setEnabled(True);

        # Attaque
        self.ui.pushButton_ability1.setEnabled(True);
        self.ui.pushButton_ability2.setEnabled(True);
        self.ui.pushButton_ability3.setEnabled(True);
        self.ui.pushButton_ability4.setEnabled(True);
        self.ui.pushButton_cancel.setEnabled(True);

        # Pokemon
        self.ui.pushButton_pokemon1.setEnabled(True);
        self.ui.pushButton_pokemon2.setEnabled(True);
        self.ui.pushButton_pokemon3.setEnabled(True);
        self.ui.pushButton_pokemon4.setEnabled(True);
        self.ui.pushButton_pokemon5.setEnabled(True);
        self.ui.pushButton_pokemon6.setEnabled(True);
        self.ui.pushButton_cancel2.setEnabled(True);

    def PutButtonsOff(self):
        # Menu
        self.ui.pushButton_Attaque.setEnabled(False);
        self.ui.pushButton_Sac.setEnabled(False);
        self.ui.pushButton_Fuite.setEnabled(False);
        self.ui.pushButton_Pokemon.setEnabled(False);

        # Sac
        self.ui.pushButton_objet1.setEnabled(False);
        self.ui.pushButton_objet2.setEnabled(False);
        self.ui.pushButton_objet3.setEnabled(False);
        self.ui.pushButton_objet4.setEnabled(False);
        self.ui.pushButton_objet5.setEnabled(False);
        self.ui.pushButton_objet6.setEnabled(False);
        self.ui.pushButton_objet7.setEnabled(False);
        self.ui.pushButton_objet8.setEnabled(False);
        self.ui.pushButton_cancel3.setEnabled(False);

        # Attaque
        self.ui.pushButton_ability1.setEnabled(False);
        self.ui.pushButton_ability2.setEnabled(False);
        self.ui.pushButton_ability3.setEnabled(False);
        self.ui.pushButton_ability4.setEnabled(False);
        self.ui.pushButton_cancel.setEnabled(False);

        # Pokemon
        self.ui.pushButton_pokemon1.setEnabled(False);
        self.ui.pushButton_pokemon2.setEnabled(False);
        self.ui.pushButton_pokemon3.setEnabled(False);
        self.ui.pushButton_pokemon4.setEnabled(False);
        self.ui.pushButton_pokemon5.setEnabled(False);
        self.ui.pushButton_pokemon6.setEnabled(False);
        self.ui.pushButton_cancel2.setEnabled(False);

# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#
#     inv = CombatWidget()
#     Pokemon = Pokemon()
#     Inventory = Inventory()
#
#     # Chargement Pokemons
#     inv.listePokemon = Pokemon.showActivePokemons()
#
#     # Chargement Pokemon Adverse
#     inv.pokemon_adverse = Pokemon.showOneRand()
#
#     inv.loadInformation()
#     inv.show()
#
#     sys.exit(app.exec_())