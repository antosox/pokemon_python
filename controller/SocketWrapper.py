#coding: utf-8

import socket
from controller.GameController import Game

class SocketWrapper:

    def __init__(self, hote, port):
        self.hote = hote
        self.port = port
        self.socket = None

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.hote, self.port))

    def close(self):
            self.socket.close()

    def write(self, msg):
        self.socket.send(msg.encode())

    def read(self):
        return self.socket.recv(255).decode()

if __name__ == "__main__":

    #sock = SocketWrapper("127.0.0.1", 8)
    #sock.connect()
    #sock.write("Salut")

    #sock.close()
    g = Game()
