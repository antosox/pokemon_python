import sys
import random


from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon, QPixmap, QImage, QFont
from PyQt5.QtWidgets import (QApplication, QLabel, QPushButton,
                             QVBoxLayout, QWidget, QListView, QTableView, QTableWidgetItem, QMessageBox)
from PyQt5.QtCore import Qt, QAbstractTableModel, QVariant, QSize
from PyQt5.uic.properties import QtGui

from content.interface.py.TraderV2 import Ui_Form
from model import Inventory,Player
from PyQt5 import QtGui, QtCore


class ListView(QWidget):


    def resizeEvent(self, a0: QtGui.QResizeEvent):

        self.pixmap = self.pixmap1.scaled(self.width()/2, self.height())
        self.label.setPixmap(self.pixmap)
        self.label.resize(self.width()/2, self.height())

    def resizeEvent2(self, a0: QtGui.QResizeEvent):

        self.pixmap2 = self.pixmap2.scaled(550, 100)
        self.label2.setPixmap(self.pixmap2)
        self.label2.resize(550, 100)



    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.resize(1100, 800)

        self.pixmap1 = QtGui.QPixmap('./../content/menu/trader.png')
        self.label = QLabel(self)
        self.resizeEvent(self.pixmap1)

        self.pixmap2 = QtGui.QPixmap('./../content/menu/trader2.png')
        self.label2 = QLabel(self)
        self.resizeEvent2(self.pixmap2)
        self.label2.move(550,25)
        self.label3 = QLabel(self)
        self.player = Player.Player("Sasha")
        self.solde = self.player.getSolde()
        self.label3.move(750, 715)
        self.label3.setText("Solde :"+str(self.solde))
        self.label3.setStyleSheet("font-size:30px")
        self.show()
        row = 0
        column = 0
        self.my_array = [['Potion', '200', 'potion.png'], ['Super Potion', '700', 'super-potion.png'],
                    ['Hyper Potion', '1500', 'hyper-potion.png'], ['Potion Max', '2500', 'max-potion.png'],
                    ['Max Revive', '3000', 'potion.png'], ['PokeBall', '100', 'poke-ball.png'],
                    ['SuperBall', '300', 'super-ball.png'], ['HyperBall', '500', 'hyper-ball.png']]
        taille = len(self.my_array)

        self.ui.tableWidget.setRowCount(taille)
        self.ui.tableWidget
        self.ui.tableWidget.setStyleSheet("margin-top:100; border:0px;padding-top:20;")
        fnt = self.ui.tableWidget.horizontalHeader().font()
        fnt.setPointSize(12);
        fnt.setFamily("Courier New");
        self.ui.tableWidget.setFont(fnt)


        for var in self.my_array:
            # label.setPixmap(QPixmap("../content/image/icon/potion.png"))
            # item =QTableWidgetItem
            test = QTableWidgetItem("")
            icon = QIcon('../content/image/icon/' + str(var[2]))

            test.setIcon(icon)
            self.ui.tableWidget.setIconSize(QSize(50,50))
            self.ui.tableWidget.setItem(row, column, test)

            # self.ui.tableWidget.setItem(rowXSXS, column, item)
            column = column + 1

            self.ui.tableWidget.setItem(row, column, QTableWidgetItem(str(var[0])))
            column = column + 1
            self.ui.tableWidget.setItem(row, column, QTableWidgetItem(str(var[1])))
            column = column + 1
            self.ui.tableWidget.setItem(row, column, QTableWidgetItem("Acheter"))
            column = 0
            self.ui.tableWidget.setRowHeight(row, 70)
            self.ui.tableWidget.setColumnWidth(0,100)
            self.ui.tableWidget.setColumnWidth(1,150)
            self.ui.tableWidget.setColumnWidth(2,130)
            self.ui.tableWidget.setColumnWidth(3,150)
            row = row + 1

            self.ui.tableWidget.show()
        # Create the QTableView widget and associate to its model
        # tablemodel = MyTableModel(self.my_array, self)
        # print(tablemodel)
        #
        for column in range(8):
            for row in range(4):
                self.ui.tableWidget.item(0+column,0+row).setTextAlignment(Qt.AlignCenter)
        self.ui.tableWidget.verticalHeader().setVisible(False)
        # self.ui.tableWidget.horizontalHeader().setVisible(False)
        self.ui.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem(""));
        self.ui.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Nom"));
        self.ui.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Prix"));
        self.ui.tableWidget.setHorizontalHeaderItem(3, QTableWidgetItem(""));

        # The raw data
        self.func_mappingSignal()

    def func_mappingSignal(self):
        self.ui.tableWidget.doubleClicked.connect(self.func_buy)

    def func_buy(self, item):
        if (item.column() == 3):
            object = self.my_array[item.row()][item.column() - 3]
            price = self.my_array[item.row()][item.column() - 2]
            self.func_debit(price,object)

    def func_debit(self, price,object):
        player = Player.Player("Sasha")
        solde = player.getSolde()
        if solde - int(price) >= 0 : # achat possible
            solde = solde - int(price)
            inv = Inventory.Inventory("nana")
            if str(object).__contains__("Potion"):
                inv.majInventory("potions", str(object), 1)
                player.updateSolde(solde)
            if str(object).__contains__("Ball"):
                inv.majInventory("pokeballs",str(object), 1)
                player.updateSolde(solde)
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Achat réalisé")
            msg.setStandardButtons(QMessageBox.Ok)
            retval = msg.exec_()
            self.label3.setText("Solde :" + str(solde))

        else : # achat impossible
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setText("Solde insuffisant")
            msg.setStandardButtons(QMessageBox.Ok)
            retval = msg.exec_()

class MyTableModel(QAbstractTableModel):

    def __init__(self, datain, parent=None):
        QAbstractTableModel.__init__(self, parent)
        self.arraydata = datain

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        return len(self.arraydata[0])

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role == Qt.EditRole:
            return None
        elif role != Qt.DisplayRole:
            return None
        return self.arraydata[index.row()][index.column()]


# if __name__ == "__main__":
    # app = QApplication(sys.argv)
    # self = ListView()
    # self.show()
    # row = 0
    # column = 0
    # self.my_array = [['Potion', '200','potion.png'],['Super Potion', '700','super-potion.png'],['Hyper Potion', '1500','hyper-potion.png'], ['Potion Max', '2500','max-potion.png'],['Max Revive', '3000','potion.png'],['PokeBall', '100','poke-ball.png'],['SuperBall', '300','super-ball.png'],['HyperBall','500','hyper-ball.png']]
    # taille = len(self.my_array)
    #
    # self.ui.tableWidget.setRowCount(taille)
    # self.ui.tableWidget
    # for var in self.my_array:
    #
    #     #label.setPixmap(QPixmap("../content/image/icon/potion.png"))
    #     #item =QTableWidgetItem
    #     test = QTableWidgetItem("")
    #     icon = QIcon('../content/image/icon/'+str(var[2]))
    #     test.setIcon(icon)
    #
    #     self.ui.tableWidget.setItem(row,column,test)
    #
    #     #self.ui.tableWidget.setItem(row, column, item)
    #     column = column + 1
    #     self.ui.tableWidget.setItem(row, column, QTableWidgetItem(str(var[0])))
    #     column = column + 1
    #     inv.ui.tableWidget.setItem(row, column, QTableWidgetItem(str(var[1])))
    #     column = column + 1
    #     inv.ui.tableWidget.setItem(row, column, QTableWidgetItem("Acheter"))
    #     column = 0
    #     row = row + 1
    #     inv.ui.tableWidget.show()
    # # Create the QTableView widget and associate to its model
    # # tablemodel = MyTableModel(self.my_array, inv)
    # #
    # inv.ui.tableWidget.verticalHeader().setVisible(False)
    # # inv.ui.tableWidget.horizontalHeader().setVisible(False)
    # inv.ui.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem(""));
    # inv.ui.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Nom"));
    # inv.ui.tableWidget.setHorizontalHeaderItem(2, QTableWidgetItem("Prix"));
    # inv.ui.tableWidget.setHorizontalHeaderItem(3, QTableWidgetItem(""));
    # # The raw data
    # inv.func_mappingSignal()
    # sys.exit(app.exec_())
