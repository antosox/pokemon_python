from PyQt5.uic.properties import QtCore
import sys
from content.interface.py.MpConnexion import Ui_Dialog
from PyQt5.QtWidgets import QDialog
import PyQt5


class MpConnectionDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        dialog = QDialog()
        self.ui = Ui_Dialog()
        self.ui.setupUi(dialog)
        self.ui.pushButton_Close.clicked.connect(dialog.close)
        dialog.exec_()

    def getUi(self):
        return self.ui

if __name__ == "__main__":
    dialog = QDialog()
    mp = MpConnectionDialog()
