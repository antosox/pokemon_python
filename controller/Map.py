import sys
import random

from PyQt5.QtCore import QThread
from PyQt5.QtGui import QPalette, QBrush, QPixmap, QImage
from PyQt5.QtWidgets import (QApplication, QLabel, QPushButton,
                             QVBoxLayout, QWidget, QGridLayout, QListWidget)
from PyQt5.QtGui import QPixmap
from PyQt5.Qt import Qt
from PyQt5 import QtGui
import PyQt5

from controller import TraderController, CombatController, InventoryController, PokedexController


class MyWidget(QWidget):



    def resizeEvent(self, a0: QtGui.QResizeEvent):

        self.pixmap = self.pixmap1.scaled(self.width(), self.height())
        self.label.setPixmap(self.pixmap)
        self.label.resize(self.width(), self.height())

    def __init__(self):
        QWidget.__init__(self)
        self.pixmap1 = QtGui.QPixmap('./../content/image/map/AA.png')
        self.label = QLabel(self)
        self.label.resize(1500, 800)
        self.compteur = 0
        self.map = 'AA_H'
        self.lamap=''
        self.onMap = False
        self.moves = False
        self.start = False
        self.taillebtn= 1000/30

        # pixmap1 = QPixmap('./../content/image/map/AA.png')
        # self.pixmap = pixmap1.scaled(self.width(), self.height())
        # self.label.setPixmap(QPixmap('./../content/image/map/Map.png'))
        # self.label.setPixmap(self.pixmap)
        self.label.setMinimumSize(1, 1)
        self.label.setGeometry(0,0,1200,1200)
        self.move(200,50)
        self.label.hasScaledContents()
        self.label.isFullScreen()
        self.label.adjustSize()
        self.comp = 0
        self.grid_layout = QGridLayout()
        self.setLayout(self.grid_layout)
        self.positionJoueur = [12, 7]
        self.taille = 30
        self.values1 = []
        self.values = []
        self.obstacles = []
        self.grid_layout.setHorizontalSpacing(1)
        self.grid_layout.setVerticalSpacing(1)
        self.grid_layout.setSpacing(0)
        self.resizeEvent(self.pixmap1)
        for u in range(self.taille):
            self.values1.append(' ')
        for i in range(self.taille):
            self.values.append(self.values1.copy())
            self.obstacles.append(self.values1.copy())
        self.MapStart()



    def keyPressEvent(self, event):
        self.moves = False
        positionavant = [self.positionJoueur[0], self.positionJoueur[1]]
        self.lamap = self.map.split('_')
        self.values[self.positionJoueur[0]][self.positionJoueur[1]] = ''

        self.compteur += 1
        if self.onMap == False:
            if event.key() == Qt.Key_Z:
                self.moves = True
                self.positionJoueur[0] = self.positionJoueur[0] - 1
                if self.compteur %2 == 1:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Haut1.png) no-repeat center center fixed transparent; ")
                else:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Haut2.png) no-repeat center center fixed transparent; ")
            if event.key() == Qt.Key_D:
                self.moves = True
                self.positionJoueur[1] = self.positionJoueur[1] + 1
                if self.compteur %2 == 1:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Droite1.png) no-repeat center center fixed transparent; ")
                else:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Droite2.png) no-repeat center center fixed transparent; ")
            if event.key() == Qt.Key_S:
                self.moves = True
                self.positionJoueur[0] = self.positionJoueur[0] + 1
                if self.compteur %2 == 1:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Bas1.png) no-repeat center center fixed transparent; ")
                else:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Bas2.png) no-repeat center center fixed transparent; ")
            if event.key() == Qt.Key_Q:
                self.moves = True
                self.positionJoueur[1] = self.positionJoueur[1] - 1
                if self.compteur %2 == 1:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Gauche1.png) no-repeat center center fixed transparent; ")
                else:
                    butz = QPushButton("X")
                    butz.setStyleSheet("background:url(./../content/image/map/Joueur/Gauche2.png) no-repeat center center fixed transparent; ")

        if event.key() == Qt.Key_I:
            self.inventaire = InventoryController.InventoryWidget()
        if event.key() == Qt.Key_P:
            self.pokedex = PokedexController.PokedexWidget()

        if event.key() == Qt.Key_M:
            self.grid_layout.itemAtPosition(self.positionJoueur[0], self.positionJoueur[1]).widget().deleteLater()
            if self.onMap== False:

                self.onMap = True


                self.pixmap1 = QtGui.QPixmap('./../content/image/map/Localisation/'+self.lamap[0]+'.png')
                self.resizeEvent(self.pixmap1)
                self.initMap()
                self.intiGrille()
            else:
                self.onMap = False

                self.pixmap1 = QtGui.QPixmap('./../content/image/map/'+self.lamap[0]+'.png')
                self.resizeEvent(self.pixmap1)
                self.initMap()
                if self.lamap[0] == 'AA':
                    self.MapAA()
                if self.lamap[0] == 'AB':
                    self.MapAB()
                if self.lamap[0] == 'AC':
                    self.MapAC()
                if self.lamap[0] == 'AD':
                    self.MapAD()
                if self.lamap[0] == 'BA':
                    self.MapBA()
                if self.lamap[0] == 'BB':
                    self.MapBB()
                if self.lamap[0] == 'BC':
                    self.MapBC()
                if self.lamap[0] == 'BD':
                    self.MapBD()
                if self.lamap[0] == 'CA':
                    self.MapCA()
                if self.lamap[0] == 'CB':
                    self.MapCB()
                if self.lamap[0] == 'CC':
                    self.MapCC()
                if self.lamap[0] == 'CD':
                    self.MapCD()
                if self.lamap[0] == 'DA':
                    self.MapDA()
                if self.lamap[0] == 'DB':
                    self.MapDB()
                if self.lamap[0] == 'DC':
                    self.MapDC()
                if self.lamap[0] == 'DD':
                    self.MapDD()


                self.intiGrille()

        if self.positionJoueur[0] == self.taille:
            self.positionJoueur[0] = 0
        if self.positionJoueur[0] == -1:
            self.positionJoueur[0] = self.taille - 1
        if self.positionJoueur[1] == self.taille:
            self.positionJoueur[1] = 0
        if self.positionJoueur[1] == -1:
            self.positionJoueur[1] = self.taille - 1

        if self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'O':
            self.positionJoueur = [positionavant[0], positionavant[1]]

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AA_D':
            self.map='AA_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AA_B':
            self.map = 'AA_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAA()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BA_G':
            self.map = 'BA_G'
            self.positionJoueur=[self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BA_B':
            self.map = 'BA_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BA_D':
            self.map = 'BA_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CA_G':
            self.map = 'CA_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CA_B':
            self.map = 'CA_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CA_D':
            self.map = 'CA_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCA()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DA_G':
            self.map = 'DA_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDA()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DA_B':
            self.map = 'DA_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DA.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDA()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AB_H':
            self.map = 'AB_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AB_D':
            self.map = 'AB_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AB_B':
            self.map = 'AB_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAB()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BB_G':
            self.map = 'BB_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BB_H':
            self.map = 'BB_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BB_B':
            self.map = 'BB_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BB_D':
            self.map = 'BB_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBB()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CB_B':
            self.map = 'CB_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CB_H':
            self.map = 'CB_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CB_D':
            self.map = 'CB_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CB_G':
            self.map = 'CB_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCB()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DB_G':
            self.map = 'DB_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DB_H':
            self.map = 'DB_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDB()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DB_B':
            self.map = 'DB_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DB.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDB()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AC_H':
            self.map = 'AC_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AC_B':
            self.map = 'AC_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAC()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BC_H':
            self.map = 'BC_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'BC_D':
            self.map = 'BC_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/BC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapBC()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CC_G':
            self.map = 'CC_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CC_H':
            self.map = 'CC_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CC_B':
            self.map = 'CC_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CC_D':
            self.map = 'CC_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCC()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DC_G':
            self.map = 'DC_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DC_H':
            self.map = 'DC_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDC()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DC_B':
            self.map = 'DC_B'
            self.positionJoueur = [28, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DC.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDC()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'AD_H':
            self.map = 'AD_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/AD.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapAD()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CD_H':
            self.map = 'CD_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CD.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCD()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'CD_D':
            self.map = 'CD_D'
            self.positionJoueur=[self.positionJoueur[0], 28]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/CD.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapCD()
            self.intiGrille()

        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DD_G':
            self.map = 'DD_G'
            self.positionJoueur = [self.positionJoueur[0], 1]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DD.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDD()
            self.intiGrille()
        elif self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'DD_H':
            self.map = 'DD_H'
            self.positionJoueur = [1, self.positionJoueur[1]]
            self.pixmap1 = QtGui.QPixmap('./../content/image/map/DD.png')
            self.resizeEvent(self.pixmap1)
            self.initMap()
            self.MapDD()
            self.intiGrille()

        else:
            if self.moves == True:
                if self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'E':
                    self.leRand = random.randint(0, 4)
                    if self.leRand == 2:
                        combat = CombatController.CombatWidget()
                if self.obstacles[self.positionJoueur[0]][self.positionJoueur[1]] == 'M':
                    self.marchand = TraderController.ListView()


                self.grid_layout.itemAtPosition(positionavant[0], positionavant[1]).widget().deleteLater()

                but = QPushButton(" ")
                but.setFixedWidth(self.taillebtn)
                but.setStyleSheet("background-color: transparent")

                self.grid_layout.addWidget(but, positionavant[0], positionavant[1])
                self.grid_layout.itemAtPosition(positionavant[0], positionavant[1]).widget().update()

                self.grid_layout.itemAtPosition(self.positionJoueur[0], self.positionJoueur[1]).widget().deleteLater()

                butz.setFixedWidth(self.taillebtn)
                self.grid_layout.addWidget(butz, self.positionJoueur[0], self.positionJoueur[1])
                self.grid_layout.itemAtPosition(self.positionJoueur[0], self.positionJoueur[1]).widget().update()

        self.values[self.positionJoueur[0]][self.positionJoueur[1]] = 'X'






    def initMap(self):
        self.pixmap = self.pixmap1.scaled(self.width(), self.height())
        self.label.setPixmap(self.pixmap)
        self.label.resize(self.width(), self.height())
        # self.resize(1100, 1000)
        for i in range(self.taille):
            for u in range(self.taille):
                self.grid_layout.itemAtPosition(u, i).widget().deleteLater()
                self.obstacles[u][i] = " "

        # for u in range(self.taille):
        #     for i in range(self.taille):
        #         buttossn = QPushButton(self.values[u][i])
        #         button.setObjectName(str(u) + "," + str(i))
        #         if self.obstacles[u][i] == 'O':
        #             button.setStyleSheet("background-color: red")
        #         elif self.obstacles[u][i] == 'E':
        #             button.setStyleSheet("background-color: green")
        #         else:
        #             button.setStyleSheet("background-color: transparent")
        #         self.grid_layout.addWidget(button, u, i)
        #         i += 1
        #     u += 1

    def intiGrille(self):
        self.lamap = self.map.split('_')
        for u in range(self.taille):
            for i in range(self.taille):
                button = QPushButton(self.values[u][i])
                button.setObjectName(str(u) + "," + str(i))
                # if self.obstacles[u][i] == 'O':
                #     button.setStyleSheet("background-color: red")
                button.setFixedWidth(self.taillebtn)
                if self.obstacles[u][i] == 'E':
                    button.setStyleSheet("background-color: transparent")
                elif self.obstacles[u][i] == 'M':
                    button.setStyleSheet("background-color: transparent")
                elif u == self.positionJoueur[0] and i == self.positionJoueur[1]:
                    if self.onMap==False :
                        if self.lamap[1] == "H":
                            button.setStyleSheet("background:url(./../content/image/map/Joueur/Bas2.png) no-repeat center center fixed transparent; ")
                        elif self.lamap[1] == "B":
                            button.setStyleSheet("background:url(./../content/image/map/Joueur/Haut2.png) no-repeat center center fixed transparent; ")
                        elif self.lamap[1] == "D":
                            button.setStyleSheet("background:url(./../content/image/map/Joueur/Gauche2.png) no-repeat center center fixed transparent; ")
                        elif self.lamap[1] == "G":
                            button.setStyleSheet("background:url(./../content/image/map/Joueur/Droite2.png) no-repeat center center fixed transparent; ")
                        else:
                            button.setStyleSheet("background:url(./../content/image/map/Joueur/Bas2.png) no-repeat center center fixed transparent; ")

                else:
                    button.setStyleSheet("background-color: transparent")


                self.grid_layout.addWidget(button, u, i)
                # self.grid_layout.itemAtPosition(u,i).widget().setObjectName(str(u)+","+str(i))
                # self.grid_layout.itemAtPosition(u,i).widget().setFixedWidth(0)
                # self.grid_layout.itemAtPosition(u,i).widget().setFixedHeight(0)
            #     i += 1
            # u += 1


    def MapStart(self):

        for u in range(12):
            for i in range(self.taille):
                self.obstacles[u][i]= 'O'
        for u in range(self.taille):
            for i in range(7):
                self.obstacles[u][i] = 'O'
        for u in range(4):
            self.obstacles[11][14+u] = ' '
            self.obstacles[10][14+u] = ' '
        self.obstacles[9][14]=' '
        self.obstacles[9][15]=' '
        for u in range(5):
            for i in range(11):
                self.obstacles[19+u][7+i] = 'O'
        for u in range(2):
            for i in range(6):
                self.obstacles[u+17][i+7] = 'O'
        for u in range(6):
            for i in range(3):
                self.obstacles[u+24][i+7] = 'O'
        for u in range(8):
            for i in range(9):
                self.obstacles[u+16][i+19]='O'
        for i in range(5):
            self.obstacles[i + 19][18] = 'O'
        for i in range (13):
            self.obstacles[29][i + 17] = 'O'
        for i in range(17):
            self.obstacles[12+i][29]='BA_G'
        for i in range(7):
            self.obstacles[29][10+i]='AB_H'
        self.values[12][7] = 'p'
        self.intiGrille()
        self.map = "AA_B"
        self.start = True
        self.show()

    def MapAA(self):
        for u in range(12):
            for i in range(self.taille):
                self.obstacles[u][i]= 'O'
        for u in range(self.taille):
            for i in range(7):
                self.obstacles[u][i] = 'O'
        for u in range(4):
            self.obstacles[11][14+u] = ' '
            self.obstacles[10][14+u] = ' '
        self.obstacles[9][14]=' '
        self.obstacles[9][15]=' '
        for u in range(5):
            for i in range(11):
                self.obstacles[19+u][7+i] = 'O'
        for u in range(2):
            for i in range(6):
                self.obstacles[u+17][i+7] = 'O'
        for u in range(6):
            for i in range(3):
                self.obstacles[u+24][i+7] = 'O'
        for u in range(8):
            for i in range(9):
                self.obstacles[u+16][i+19]='O'
        for i in range(5):
            self.obstacles[i + 19][18] = 'O'
        for i in range (13):
            self.obstacles[29][i + 17] = 'O'
        for i in range(17):
            self.obstacles[12+i][29]='BA_G'
        for i in range(7):
            self.obstacles[29][10+i]='AB_H'

    def MapBA(self):
        for i in range(5):
            for u in range(3):
                self.obstacles[0+i][0+u]='O'
        for u in range(5):
            for i in range(2):
                self.obstacles[5 + u][0 + i] = 'O'
        self.obstacles[11][0] = 'O'
        self.obstacles[10][0] = 'O'
        for u in range(5):
            for i in range(2):
                self.obstacles[0 + u][3 + i] = 'O'
        self.obstacles[5][3] = 'O'
        self.obstacles[5][2] = 'O'
        for i in range(3):
            self.obstacles[9][2 + i] = 'O'
        for i in range(10):
            self.obstacles[0][5+i]= 'O'
        for u in range(6):
            for i in range(15):
                self.obstacles[0 + u][15 + i] = 'O'
        for u in range(6):
            for i in range(12):
                self.obstacles[6 + u][18 + i] = 'O'
        for i in range(5):
            self.obstacles[9][13+ i] = 'O'
        for u in range(7):
            for i in range(11):
                self.obstacles[12 + u][18 + i] = 'O'
        for u in range(2):
            for i in range(9):
                self.obstacles[18 + u][19 + i] = 'O'
        for u in range(4):
            for i in range(7):
                self.obstacles[20 + u][22 + i] = 'O'
        for u in range(2):
            for i in range(9):
                self.obstacles[22 + u][13 + i] = 'O'
        for i in range(3):
            self.obstacles[21][13 + i] = 'O'
        for i in range(14):
            self.obstacles[29][0 + i] = 'O'
        for i in range(9):
            self.obstacles[29][21 + i] = 'O'
        for u in range(6):
            for i in range(5):
                self.obstacles[18 + u][2 + i] = 'O'
        for i in range(17):
            self.obstacles[12 + i][0] = 'AA_D'
        for i in range(7):
            self.obstacles[29][14+i]= 'BB_H'
        for i in range(17):
            self.obstacles[12+i][29]= 'CA_G'


    def MapAB(self):
        for u in range(7):
            for i in range(30):
                self.obstacles[i][0+u]= 'O'
        for u in range(19):
            for i in range(2):
                self.obstacles[11+u][7+i]= 'O'
        for i in range(9):
            self.obstacles[13 + i][9] = 'O'
        for u in range(4):
            for i in range(3):
                self.obstacles[0+u][7+i]= 'O'
        self.obstacles[7][7] = 'O'
        for i in range(4):
            self.obstacles[29][9 + i] = 'O'
        for u in range(3):
            for i in range(2):
                self.obstacles[26+u][9+i]= 'O'
        for u in range(4):
            for i in range(12):
                self.obstacles[0+u][18+i]= 'O'
        for u in range(13):
            for i in range(10):
                self.obstacles[4+u][20+i]= 'O'
        for i in range(5):
            self.obstacles[12 + i][20 ] = ' '
        for u in range(7):
            for i in range(4):
                self.obstacles[6+u][17+i]= 'O'
        for u in range(8):
            for i in range(13):
                self.obstacles[22+u][17+i]= 'O'
        for u in range(2):
            for i in range(9):
                self.obstacles[20+u][21+i]= 'O'
        for i in range(8):
            self.obstacles[0][10 + i] = 'AA_B'
        self.obstacles[29][13] = 'O'
        for u in range(3):
            self.obstacles[17+u][29]='BB_G'
        for u in range(3):
            self.obstacles[29][14+u]='AC_H'



    def MapBB(self):
        for u in range(4):
            for i in range(15):
                self.obstacles[0 + u][0 + i] = 'O'
        for u in range(5):
            for i in range(8):
                self.obstacles[3+u][8+i]= 'O'
        for u in range(8):
            for i in range(5):
                self.obstacles[4+u][0+i]= 'O'
        for u in range(5):
            for i in range(2):
                self.obstacles[12+u][0+i]= 'O'
        for u in range(4):
            for i in range(2):
                self.obstacles[20+u][0+i]= 'O'
        for u in range(4):
            for i in range(3):
                self.obstacles[20+u][3+i]= 'O'
        for u in range(3):
            for i in range(3):
                self.obstacles[22+u][5+i]= 'O'
        for i in range(3):
            self.obstacles[29][13 + i] = 'O'
        for i in range(2):
            self.obstacles[21 + i][17] = 'O'
        for u in range(7):
            for i in range(12):
                self.obstacles[13+u][18+i]= 'O'
        for u in range(2):
            for i in range(9):
                self.obstacles[20+u][21+i]= 'O'
        for i in range(2):
            self.obstacles[12][28 + i] = 'O'
        self.obstacles[11][29] = 'O'
        self.obstacles[0][21] = 'O'
        for u in range(8):
            for i in range(8):
                self.obstacles[0+u][22+i]= 'O'
        for i in range(4):
            self.obstacles[8][25 + i] = 'O'
        for i in range(4):
            self.obstacles[8+i][29] = 'O'
        for u in range(7):
            for i in range(2):
                self.obstacles[13+u][16+i]= 'O'
        for i in range(10):
            self.obstacles[20 +i][19] = 'O'
        for i in range(6):
            self.obstacles[6][16+i] = 'O'
        for i in range(6):
            self.obstacles[24+i][0] = 'O'
        for i in range(6):
            self.obstacles[0][15+i] = 'BA_B'
        for i in range(3):
            self.obstacles[17+i][0] = 'AB_D'
        for i in range(11):
            self.obstacles[29][1+i] = 'BC_H'
        for u in range(3):
            self.obstacles[29][16+u]='BC_H'
        for u in range(3):
            self.obstacles[27+u][29]='CB_G'
        for u in range(9):
            self.obstacles[29][20+u]='BC_H'
        for u in range(6):
            for i in range(8):
                self.obstacles[23 + u][21 + i] = 'E'
        self.obstacles[8][9] = 'M'
        self.obstacles[8][10] = 'M'
        self.obstacles[8][24] = 'M'
        self.obstacles[8][25] = 'M'





    def MapAC(self):
        for u in range(30):
            for i in range(7):
                self.obstacles[0 + u][0 + i] = 'O'
        for u in range(6):
            for i in range(7):
                self.obstacles[0+u][7+i]= 'O'
        self.obstacles[3][14] = 'O'
        for u in range(27):
            for i in range(12):
                self.obstacles[0+u][18+i]= 'O'
        for u in range(3):
            for i in range(10):
                self.obstacles[27+u][20+i]= 'O'
        for u in range(14):
            for i in range(5):
                self.obstacles[11+u][7+i]= 'O'
        self.obstacles[20][12] = 'O'
        self.obstacles[21][12] = 'O'
        for u in range(6):
            for i in range(4):
                self.obstacles[13+u][13+i]= 'O'
        for u in range(4):
            self.obstacles[15 + u][13] = ' '
        for u in range(10):
            self.obstacles[14 + u][17] = 'O'
        for u in range(6):
            self.obstacles[19 + u][16] = 'O'
        self.obstacles[19][15] = 'O'
        self.obstacles[22][15] = 'O'
        self.obstacles[23][15] = 'O'
        for u in range(5):
            for i in range(3):
                self.obstacles[25+u][7+i]= 'O'
        for i in range(10):
            self.obstacles[29][10 + i] = 'AD_H'
        for i in range(4):
            self.obstacles[0][14 + i] = 'AB_B'
        for u in range(2):
            for i in range(5):
                self.obstacles[26 + u][13 + i] = 'E'
        for i in range(8):
            self.obstacles[28][12 + i] = 'E'
        self.obstacles[6][8] = 'M'


    def MapAD(self):
        for u in range(30):
            for i in range(9):
                self.obstacles[0 + u][0 + i] = 'O'
        for u in range(20):
            for i in range(6):
                self.obstacles[10 + u][9 + i] = 'O'
        for u in range(15):
            for i in range(15):
                self.obstacles[15 + u][15 + i] = 'O'
        for u in range(15):
            for i in range(5):
                self.obstacles[0 + u][25 + i] = 'O'
        for u in range(10):
            for i in range(2):
                self.obstacles[0 + u][23 + i] = 'O'
        for u in range(5):
            for i in range(2):
                self.obstacles[4 + u][21 + i] = 'O'
        for i in range(4):
            self.obstacles[9][9 + i] = 'O'
        for i in range(3):
            self.obstacles[14][22 + i] = 'O'
        for i in range(3):
            self.obstacles[0][20 + i] = 'O'
        for i in range(11):
            self.obstacles[0][9 + i] = 'AC_B'
        for u in range(6):
            for i in range(11):
                self.obstacles[1 + u][9 + i] = 'E'
        for u in range(5):
            self.obstacles[2 + u][20] = 'E'
        for u in range(2):
            for i in range(8):
                self.obstacles[7 + u][10 + i] = 'E'
        self.obstacles[9][13] = 'E'
        self.obstacles[9][14] = 'E'
        for i in range(5):
            self.obstacles[11][18 + i] = 'E'
        for u in range(3):
            for i in range(10):
                self.obstacles[12 + u][15 + i] = 'E'

    def MapBC(self):
        for i in range(27):
            self.obstacles[3+i][0] = 'O'
        for i in range(21):
            self.obstacles[9+i][1] = 'O'
        for i in range(20):
            self.obstacles[10+i][2] = 'O'
        for u in range(18):
            for i in range(3):
                self.obstacles[12 + u][3 + i] = 'O'
        for u in range(17):
            for i in range(2):
                self.obstacles[13 + u][6 + i] = 'O'
        for u in range(14):
            for i in range(3):
                self.obstacles[16 + u][8 + i] = 'O'
        for u in range(4):
            for i in range(5):
                self.obstacles[26 + u][11 + i] = 'O'
        for u in range(14):
            for i in range(14):
                self.obstacles[16 + u][16 + i] = 'O'
        for u in range(9):
            for i in range(9):
                self.obstacles[8 + u][21 + i] = 'O'
        self.obstacles[10][19] = 'O'
        self.obstacles[13][17] = 'O'
        for u in range(4):
            for i in range(3):
                self.obstacles[7 + u][8 + i] = 'O'
        for u in range(5):
            for i in range(3):
                self.obstacles[0 + u][13 + i] = 'O'
        for i in range(3):
            self.obstacles[0 + i][19] = 'O'
        for i in range(10):
            self.obstacles[2][20+i] = 'O'
        for i in range(3):
            self.obstacles[0+i][0] = 'O'
        for i in range(12):
            self.obstacles[0][1+i] = 'BB_B'
        for i in range(3):
            self.obstacles[0][16+i] = 'BB_B'
        for i in range(5):
            self.obstacles[3+i][29] = 'CC_G'
        for i in range(10):
            self.obstacles[0][20+i] = 'BB_B'

    def MapCC(self):
        for i in range(3):
            self.obstacles[0 + i][0] = 'O'
        for i in range(3):
            self.obstacles[0+i][6] = 'O'
        for i in range(12):
            self.obstacles[2][7+i] = 'O'
        self.obstacles[1][18] = 'O'
        self.obstacles[0][18] = 'O'
        for u in range(2):
            for i in range(3):
                self.obstacles[0 + u][19 + i] = 'O'
        for u in range(17):
            for i in range(7):
                self.obstacles[8 + u][0 + i] = 'O'
        for u in range(5):
            for i in range(5):
                self.obstacles[25 + u][0 + i] = 'O'
        for i in range(5):
            self.obstacles[13 + i][7] = 'O'
        for u in range(20):
            for i in range(10):
                self.obstacles[10 + u][20 + i] = 'O'
        self.obstacles[10][20]='O'
        for i in range(4):
            self.obstacles[19][8 + i] = 'O'
        for i in range(5):
            self.obstacles[24][15 + i] = 'O'
        for i in range(5):
            self.obstacles[28][5 + i] = 'O'
        for i in range(5):
            self.obstacles[18+i][7] = 'O'
        for i in range(5):
            self.obstacles[0][1+i] = 'CB_B'
        for i in range(8):
            self.obstacles[0][22+i] = 'CB_B'
        for i in range(15):
            self.obstacles[29][5+i] = 'CD_H'
        for i in range(11):
            self.obstacles[0][7+i] = 'CB_B'
        for i in range(5):
            self.obstacles[3+i][0] = 'BC_D'
        for i in range(4):
            self.obstacles[1+i][29] = 'DC_G'



    def MapCB(self):
        for u in range(29):
            for i in range(4):
                self.obstacles[0 + u][0 + i] = 'O'
        for u in range(14):
            for i in range(4):
                self.obstacles[15 + u][4 + i] = 'O'
        for u in range(7):
            for i in range(15):
                self.obstacles[15 + u][8 + i] = 'O'
        for u in range(7):
            for i in range(5):
                self.obstacles[22 + u][18 + i] = 'O'
        for i in range(4):
            self.obstacles[29][18 + i] = 'O'
        for i in range(10):
            self.obstacles[7+i][13] = 'O'
        for i in range(3):
            self.obstacles[0+i][13] = 'O'
        for i in range(10):
            self.obstacles[0][3+i] = 'O'
        for i in range(9):
            self.obstacles[0+i][22] = 'O'
        for i in range(7):
            self.obstacles[8][23+i] = 'O'
        for u in range(2):
            for i in range(2):
                self.obstacles[0 + u][23 + i] = 'O'
        for u in range(5):
            for i in range(2):
                self.obstacles[0 + u][28 + i] = 'O'
        for i in range(9):
            self.obstacles[28][0+i] = ' '
        for i in range(10):
            self.obstacles[29][8+i] = 'CC_H'
        for i in range(8):
            self.obstacles[0][14+i] = 'CA_B'
        for i in range(8):
            self.obstacles[29][22+i] = 'CC_H'
        for i in range(3):
            self.obstacles[0][25+i] = 'CA_B'
        for i in range(3):
            self.obstacles[5+i][29] = 'DB_G'
        for i in range(21):
            self.obstacles[9+i][29] = 'DB_G'
        self.obstacles[28][0] = 'BB_D'
        for i in range(8):
            self.obstacles[29][0+i] = 'CC_H'
        for u in range(13):
            for i in range(8):
                self.obstacles[2 + u][4 + i] = 'E'
        for u in range(6):
            for i in range(9):
                self.obstacles[23 + u][8 + i] = 'E'
        self.obstacles[23][8] = ' '
        self.obstacles[24][8] = ' '




    def MapCA(self):
        for u in range(11):
            for i in range(30):
                self.obstacles[0 + u][0 + i] = 'O'
        for i in range(14):
            self.obstacles[11][0 + i] = 'O'
        for u in range(7):
            for i in range(12):
                self.obstacles[11 + u][18 + i] = 'O'
        for i in range(9):
            self.obstacles[24][21 + i] = 'O'
        for i in range(5):
            self.obstacles[25+i][21] = 'O'
        for i in range(10):
            self.obstacles[16][10 + i] = 'O'
        for i in range(4):
            self.obstacles[16][3 + i] = 'O'
        for i in range(6):
            self.obstacles[11+i][3] = 'O'
        for u in range(3):
            for i in range(7):
                self.obstacles[24 + u][8 + i] = 'O'
        self.obstacles[27][12] = 'O'
        for i in range(4):
            self.obstacles[29][0 + i] = 'O'
        for i in range(4):
            self.obstacles[29][22 + i] = 'O'
        for i in range(18):
            self.obstacles[11+i][0] = 'BA_D'
        for i in range(17):
            self.obstacles[29][4 + i] = 'CB_H'
        self.obstacles[29][26] = 'CB_H'
        self.obstacles[29][27] = 'CB_H'
        for i in range(4):
            self.obstacles[25+i][29] = 'DA_G'
        for i in range(6):
            self.obstacles[18+i][29] = 'DA_G'
        self.obstacles[12][8] = 'M'

    def MapDB(self):
        for i in range(30):
            self.obstacles[0+i][0] = 'CB_D'
        for i in range(30):
            self.obstacles[29][0 + i] = 'DC_H'
        for i in range(7):
            self.obstacles[0][7 + i] = 'DA_B'
        for u in range(5):
            for i in range(7):
                self.obstacles[0 + u][0 + i] = 'O'
        self.obstacles[5][5] = 'O'
        self.obstacles[8][5] = 'O'
        self.obstacles[8][0] = 'O'
        for i in range(4):
            self.obstacles[5 + i][6] = 'O'
        for u in range(30):
            for i in range(9):
                self.obstacles[0 + u][21 + i] = 'O'
        for u in range(12):
            for i in range(11):
                self.obstacles[13 + u][11 + i] = 'O'
        for u in range(10):
            for i in range(8):
                self.obstacles[15 + u][3 + i] = 'O'
        for i in range(13):
            self.obstacles[17 + i][2] = 'O'
        for u in range(5):
            for i in range(5):
                self.obstacles[25 + u][3 + i] = 'O'
        for u in range(6):
            for i in range(8):
                self.obstacles[24 + u][22 + i] = 'O'
        for u in range(10):
            for i in range(9):
                self.obstacles[2 + u][12 + i] = 'E'
        for u in range(5):
            for i in range(2):
                self.obstacles[25 + u][21 + i] = ' '
        for u in range(2):
            for i in range(13):
                self.obstacles[27 + u][9 + i] = 'E'
        self.obstacles[5][0]='M'





    def MapBD(self):
        self.obstacles[0][0] = 'O'

    def MapCD(self):
        for i in range(9):
            self.obstacles[1 + i][29] = 'DD_G'
        for i in range(30):
            self.obstacles[0][0 + i] = 'CC_B'
        for i in range(8):
            self.obstacles[10][22 + i] = 'O'
        for i in range(18):
            self.obstacles[10][0 + i] = 'O'
        for i in range(30):
            self.obstacles[0+i][0] = 'O'
        for u in range(21):
            for i in range(10):
                self.obstacles[9 + u][0 + i] = 'O'
        for u in range(10):
            for i in range(16):
                self.obstacles[20 + u][9 + i] = 'O'
        for u in range(8):
            for i in range(5):
                self.obstacles[22 + u][25 + i] = 'O'
        for i in range(5):
            self.obstacles[0][0 + i] = 'O'
        for i in range(13):
            self.obstacles[0][17 + i] = 'O'
        for i in range(5):
            self.obstacles[1][22 + i] = 'O'
        for i in range(4):
            self.obstacles[8][1 + i] = 'O'
        for u in range(8):
            for i in range(20):
                self.obstacles[12 + u][10 + i] = 'E'



    def MapDA(self):
        for i in range(6):
            self.obstacles[18 + i][0] = 'CA_D'
        for i in range(4):
            self.obstacles[25 + i][0] = 'CA_D'
        for i in range(30):
            self.obstacles[29][0+i] = 'DB_H'
        self.obstacles[0][0] = 'O'
        for u in range(11):
            for i in range(30):
                self.obstacles[0 + u][0 + i] = 'O'
        for u in range(5):
            for i in range(20):
                self.obstacles[10 + u][10 + i] = 'O'
        for u in range(2):
            for i in range(17):
                self.obstacles[15 + u][13 + i] = 'O'
        self.obstacles[18][16] = 'O'
        self.obstacles[19][16] = 'O'
        self.obstacles[17][16] = 'O'
        self.obstacles[16][14] = 'O'
        self.obstacles[17][0] = 'O'
        self.obstacles[16][10] = 'O'
        self.obstacles[15][10] = 'O'
        for u in range(2):
            for i in range(11):
                self.obstacles[17 + u][19 + i] = 'O'
        for u in range(11):
            for i in range(9):
                self.obstacles[19 + u][21 + i] = 'O'
        for u in range(4):
            for i in range(16):
                self.obstacles[26 + u][14 + i] = 'O'
        for i in range(6):
            self.obstacles[24 + i][6] = 'O'
        for i in range(6):
            self.obstacles[24][0 + i] = 'O'
        for u in range(2):
            for i in range(6):
                self.obstacles[28 + u][0 + i] = 'O'
        for i in range(6):
            self.obstacles[17][5 + i] = 'O'
        for u in range(3):
            for i in range(6):
                self.obstacles[11 + u][0 + i] = 'O'
        for u in range(7):
            for i in range(4):
                self.obstacles[19 + u][17 + i] = 'E'


    def MapDC(self):
        for i in range(5):
            self.obstacles[0 + i][0] = 'CC_D'
        for i in range(3):
            self.obstacles[29][1 + i] = 'DD_H'
        for i in range(3):
            self.obstacles[29][7 + i] = 'O'
        for i in range(3):
            self.obstacles[29][19 + i] = 'DD_H'
        # for i in range(30):
        #     self.obstacles[0][0 + i] = 'DB_B'
        for u in range(19):
            for i in range(8):
                self.obstacles[5 + u][0 + i] = 'O'
        for u in range(11):
            for i in range(22):
                self.obstacles[14 + u][8 + i] = 'O'
        for u in range(2):
            for i in range(20):
                self.obstacles[25 + u][10 + i] = 'O'
        for u in range(3):
            for i in range(6):
                self.obstacles[27 + u][24 + i] = 'O'
        self.obstacles[29][22] = 'O'
        self.obstacles[29][23] = 'O'
        for i in range(9):
            self.obstacles[29][10 + i] = 'O'
        for u in range(15):
            for i in range(7):
                self.obstacles[0 + u][23 + i] = 'O'
        for i in range(3):
            self.obstacles[27+i][29] = 'O'
        for u in range(2):
            for i in range(2):
                self.obstacles[27 + u][10 + i] = 'O'
        for i in range(14):
            self.obstacles[0][9 + i] = 'DB_B'
        for u in range(4):
            for i in range(13):
                self.obstacles[1 + u][9 + i] = 'E'
        for u in range(7):
            for i in range(13):
                self.obstacles[7 + u][9 + i] = 'E'



    def MapDD(self):
        for i in range(30):
            self.obstacles[0][0 + i] = 'DC_B'
        for i in range(30):
            self.obstacles[0+i][0] = 'CD_D'
        for u in range(30):
            for i in range(25):
                self.obstacles[0 + u][5 + i] = 'O'
        for u in range(20):
            for i in range(7):
                self.obstacles[10 + u][0 + i] = 'O'

    def reprise(self):
        self.onMap=False
        self.MapStart()



if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = MyWidget()
    widget.show()
    widget.resize(1100, 1000)
    widget.setFixedSize(1100, 1000)

    sys.exit(app.exec_())