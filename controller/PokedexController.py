import json

import sys
import urllib

from PyQt5 import QtGui
from PyQt5.QtCore import QStringListModel
from PyQt5.QtGui import QStandardItemModel, QPixmap
from PyQt5.QtWidgets import QDialog, QApplication, QAbstractItemView, QListWidget, QListWidgetItem
from PyQt5.uic.properties import QtCore

from content.interface.py.Pokedex import Ui_Dialog
from model.Pokedex import Pokedex
from model.Pokemon import Pokemon
import requests


class PokedexWidget(QDialog):

    def __init__(self, parent=None):
        QDialog.__init__(self, parent=parent)

        self.dialog = QDialog()
        # self.pokedex = Pokedex()
        # self.pokedex = pokedex
        # self.listePokemon = self.pokedex.showAll()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self.dialog)
        self.dialog.setWindowTitle("Pokedex")
        list = []
        with open('../save/pokedex.json','r') as pokedex:
            self.data = json.load(pokedex)
            for id in self.data:
                list.append(self.data[id]["name"].capitalize())

        model = QStringListModel(list)
        # self.ui.widget.setStyleSheet("background-color: #fff4e4")
        # self.setStyleSheet("background-color: red")
        # self.ui.widget_2.setStyleSheet("background-color: #fff4e4")
        self.ui.listViewPokemons.setStyleSheet("background-color: white")


        # self.ui.widget_3.setStyleSheet("background-color: #CC0000")
        self.ui.listViewDescriptions.setStyleSheet("background-color: black; color: white;")





        self.ui.listViewPokemons.setModel(model)
        test = self.showPokemonDetails(0)
        self.ui.listViewPokemons.clicked.connect(self.getSelectPKM)

        fnt = QtGui.QFont()
        fnt.setPointSize(15);

        fnt.setFamily("Courier New");
        self.ui.listViewPokemons.setFont(fnt)
        fnt2 = QtGui.QFont()
        fnt2.setPointSize(19);
        fnt2.setFamily("Courier New");
        self.ui.listViewDescriptions.setFont(fnt2)



        self.dialog.exec_()

    def colorGenerator(self, type):
        arrColor = {
            "grass": "#439c25",
            "poison": "#783a77",
            "fire": "#ee4316",
            "flying": "#90a0f1",
            "water": "#3892e8",
            "bug": "#a8b826",
            "normal": "#bfbbaf",
            "electric": "#f1c134",
            "ground": "#cfb259",
            "fairy": "#f3bbf3",
            "fighting": "#81402b",
            "psychic": "#de608a",
            "rock": "#baa25f",
            "steel": "#cbccd1",
            "ice": "#b3eafb",
            "ghost": "#605cb2",
            "dragon": "#7865dc"}

        return arrColor[type]


    def getSelectPKM(self):
        itms = self.ui.listViewPokemons.selectedIndexes()
        for id in itms:
            self.showPokemonDetails(id.row())

    def showPokemonDetails(self, id):
        id = id + 1
        # myPokemon = self.pokedex.showOne(id)

        self.loadImageURL(self.ui.labelFront, self.data[str(id)]["frontDefault"], 2)
        self.loadImageURL(self.ui.labelBack, self.data[str(id)]["backDefault"], 2)

        couleur = self.colorGenerator(str(self.data[str(id)]["types"]["type"]))

        self.ui.widget_5.setStyleSheet("background-color: "+ couleur)
        self.ui.widget_6.setStyleSheet("background-color: "+ couleur)
        self.ui.widget_4.setStyleSheet("background-color: "+ couleur)

        descriptions = []

        descriptions.append("")
        descriptions.append("  Identifiant n°"+str(id))
        descriptions.append("")
        descriptions.append("  Pokémon de type ")
        descriptions.append("  " +str(self.data[str(id)]["types"]["type"]))
        descriptions.append("")
        descriptions.append("  Disponible dans la")
        descriptions.append("  région EPSI")


        model = QStringListModel(descriptions)



        self.ui.listViewDescriptions.setModel(model)

    def loadImageURL(self, item, url, size):
        data = urllib.request.urlopen(url).read()
        pixmap = QPixmap()
        pixmap.loadFromData(data)
        pixmap2 = pixmap.scaled(pixmap.width() * size, pixmap.height() * size)
        item.setPixmap(pixmap2)

# if __name__ == '__main__':
#     # Create the Qt Application
#     app = QApplication(sys.argv)
#     # Create and show the form
#     pokedex = PokedexWidget()
#     pokedex.show()
#     # Run the main Qt loop
#     sys.exit(app.exec_())
