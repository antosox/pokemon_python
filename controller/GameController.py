import json

class Game:

    def __init__(self):
        with open('player.json', "r+") as json_data:
            self.inventory = json.load(json_data)

    def start(self):
        pass

    def getPokemons(self):
        list_pokemon = []
        for type in self.inventory['pokemons']:
            for index in type:
                list_pokemon = type[index]
        return list_pokemon
