# !/usr/bin/python37
# -*- coding: utf-8 -*-

import sys

from PyQt5.QtCore import QSize, Qt

from content.interface.py.inventory.Inventory import Ui_WidgetInventory
from PyQt5.QtWidgets import QWidget, QApplication, QTableWidgetItem
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QIcon
from model.Player import Player
import json

class InventoryWidget(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.ui = Ui_WidgetInventory()
        self.ui.setupUi(self)
        self.show()
        self.ui.pushButton_close.clicked.connect(self.close)
        self.INVENTORY = {}
        self.player = Player("Sasha")
        self.setWindowTitle("Inventaire de {}".format(self.player.getName()))
        self.loadInventory(self.player.getInventoryPlayer())


    def closeWidget(self):
        self.close()

    def loadInventory(self, inventory_dict):
        self.ui.tableWidget.setRowCount(len(inventory_dict["inventory"][0]['pokeballs'][0]))
        self.ui.tableWidget_2.setRowCount(len(inventory_dict["inventory"][0]['potions'][0]))
        self.ui.tableWidget_3.setRowCount(len(inventory_dict["inventory"][0]['object_rares'][0]))

        self.ui.tableWidget.setColumnCount(3)
        self.ui.tableWidget_2.setColumnCount(3)
        self.ui.tableWidget_3.setColumnCount(3)

        self.ui.tableWidget.verticalHeader().setVisible(False)
        self.ui.tableWidget_2.verticalHeader().setVisible(False)
        self.ui.tableWidget_3.verticalHeader().setVisible(False)

        self.ui.tableWidget.horizontalHeader().setVisible(False)
        self.ui.tableWidget_2.horizontalHeader().setVisible(False)
        self.ui.tableWidget_3.horizontalHeader().setVisible(False)

        self.ui.tableWidget.setColumnWidth(0, 70)
        self.ui.tableWidget.setColumnWidth(1, 67)
        self.ui.tableWidget.setColumnWidth(2, 230)

        self.ui.tableWidget_2.setColumnWidth(0, 60)
        self.ui.tableWidget_2.setColumnWidth(1, 65)
        self.ui.tableWidget_2.setColumnWidth(2, 220)

        self.ui.tableWidget_3.setColumnWidth(0, 70)
        self.ui.tableWidget_3.setColumnWidth(1, 67)
        self.ui.tableWidget_3.setColumnWidth(2, 230)

        fnt = self.ui.tableWidget.horizontalHeader().font()
        fnt.setPointSize(10);
        fnt.setFamily("Courier New");
        self.ui.tableWidget.setFont(fnt)
        self.ui.tableWidget_2.setFont(fnt)
        self.ui.tableWidget_3.setFont(fnt)

        self.incPotion = 0
        self.incPokeball = 0
        self.incRare = 0

        for line in inventory_dict['inventory']:
            for item in line:
                 for type in line[item]:
                    for inv in type:
                        if str(inv).__contains__("Ball"):
                            self.ui.tableWidget.setItem(self.incPotion, 1,  QTableWidgetItem(str(type[inv])))
                            self.ui.tableWidget.setItem(self.incPotion, 2,  QTableWidgetItem(inv))
                            test = QTableWidgetItem("")
                            if (inv == "PokeBall"):
                                icon = QIcon('../content/image/icon/poke-ball.png')
                            if (inv == "SuperBall"):
                                icon = QIcon('../content/image/icon/super-ball.png')
                            if (inv == "HyperBall"):
                                icon = QIcon('../content/image/icon/hyper-ball.png')
                            if (inv == "MasterBall"):
                                icon = QIcon('../content/image/icon/master-ball.png')

                            test.setIcon(icon)
                            self.ui.tableWidget.setIconSize(QSize(50, 50))
                            self.ui.tableWidget.setItem(self.incPotion, 0, test)
                            self.ui.tableWidget.setRowHeight(self.incPotion, 50)
                            self.incPotion += 1
                        elif str(inv).__contains__("Potion"):
                            self.ui.tableWidget_2.setItem(self.incPokeball, 1, QTableWidgetItem(str(type[inv])))
                            self.ui.tableWidget_2.setItem(self.incPokeball, 2, QTableWidgetItem(inv))
                            self.ui.tableWidget_2.setRowHeight(self.incPokeball, 50)
                            test2 = QTableWidgetItem("")
                            if (inv == "Potion"):
                                icon = QIcon('../content/image/icon/potion.png')
                            elif (inv == "Super Potion"):
                                icon = QIcon('../content/image/icon/super-potion.png')
                            elif (inv == "Hyper Potion"):
                                icon = QIcon('../content/image/icon/hyper-potion.png')
                            elif (inv == "Potion Max"):
                                icon = QIcon('../content/image/icon/max-potion.png')
                            elif (inv == "Potion secret"):
                                icon = QIcon('../content/image/icon/secret-potion.png')
                            else :
                                icon = QIcon('../content/image/icon/potion.png')
                                self.ui.tableWidget_2.setItem(self.incPokeball, 2, QTableWidgetItem("Potion de guérison"))


                            test2.setIcon(icon)
                            self.ui.tableWidget_2.setIconSize(QSize(40, 40))
                            self.ui.tableWidget_2.setItem(self.incPokeball, 0, test2)
                            self.incPokeball += 1
                        elif str(inv).__contains__(""):
                            self.ui.tableWidget_3.setRowHeight(self.incRare, 50)
                            self.ui.tableWidget_3.setItem(self.incRare, 1, QTableWidgetItem(str(type[inv])))
                            self.ui.tableWidget_3.setItem(self.incRare, 2, QTableWidgetItem(inv))
                            self.incRare += 1
        #
        # self.ui.listView_potion.setModel(self.model_potion)
        # self.ui.listView_pokeball.setModel(self.model_pokeball)
        # self.ui.listView_rare.setModel(self.model_rare)

    def addInventory(self, model, name, qte):
        item = QStandardItem("{} | {}".format(qte, name))
        item.setEditable(False)
        model.appendRow(item)


# if __name__ == "__main__":
#     app = QApplication(sys.argv)
#
#     inv = InventoryWidget()
#     inv.show()
#
#     sys.exit(app.exec_())